/client/verb/give_dosh(var/dosh_amount as num)
	set name = "Give Dosh"
	set category = "Cheat"

	var/mob/living/advanced/player/P = input("Who do you want to give money to?") in all_players as mob|null

	if(!P)
		return FALSE

	var/added_currency = P.adjust_currency(dosh_amount)
	to_chat("You [P.name] [added_currency] credits.")

/client/verb/set_attribute(var/mob/mob as mob)

	set name = "Set Attribute Level"
	set category = "Cheat"

	if(!is_living(mob))
		return
	var/mob/living/L = mob

	var/list/valid_choices = list()

	for(var/k in L.attributes)
		valid_choices += k

	var/chosen_attribute = input("Which attribute do you wish to modify?","Modify Attribute") as null|anything in valid_choices

	if(!chosen_attribute)
		return

	var/chosen_value = input("Which value do you wish to set [chosen_attribute] to?","Modify Attribute") as num|null

	if(!chosen_value)
		return

	chosen_value = chosen_value

	L.set_attribute_level(chosen_attribute,chosen_value)

	L << "Your [chosen_attribute] is now [L.get_attribute_level(chosen_attribute)]."


/client/verb/set_skill(var/mob/mob as mob)

	set name = "Set Skill Level"
	set category = "Cheat"

	if(!is_living(mob))
		return
	var/mob/living/L = mob

	var/list/valid_choices = list()

	for(var/k in L.skills)
		valid_choices += k

	var/chosen_skill = input("Which skill do you wish to modify?","Modify Skill") as null|anything in valid_choices

	if(!chosen_skill)
		return

	var/chosen_value = input("Which value do you wish to set [chosen_skill] to?","Modify Skill") as num|null

	if(!chosen_value)
		return

	chosen_value = chosen_value

	L.set_skill_level(chosen_skill,chosen_value)

	L << "Your [chosen_skill] is now [L.get_skill_level(chosen_skill)]."


/client/verb/rtv()
	set name = "Rock the Vote"
	set category = "Admin"
	SShorde.round_time = 1000000

/client/verb/rejuvenate_player()
	set name = "Rejuvenate Player"
	set category = "Cheat"

	var/mob/living/advanced/player/P = input("Who do you want to rejuvenate?","Player Rejuvenation") in all_players as mob|null

	if(!P)
		return FALSE

	P.resurrect()
	to_chat("You rejuvenated [P.name].")


/client/verb/force_round_end()
	set name = "Force Round End (DANGER)"
	set category = "Cheat"

	var/confirm = input("Are you sure you want to end the round in a NanoTrasen Victory?","NanoTrasen Victory") in list("Yes","Cancel")|null

	if(confirm != "Yes")
		return FALSE

	world.end(WORLD_END_NANOTRASEN_VICTORY,FALSE)

/client/verb/bring_player()
	set name = "Bring Player"
	set category = "Admin"

	sortTim(all_mobs_with_clients,/proc/cmp_path_asc)

	var/mob/choice = input("Who would you like to bring","Bring Mob") as null|mob in all_mobs_with_clients
	if(!choice)
		to_chat(span("warning","Invalid mob."))
		return FALSE

	var/turf/T = get_turf(mob)
	if(!T)
		to_chat(span("warning","Invalid turf."))
		return FALSE

	choice.force_move(T)

	to_chat(span("notice","You brought \the [choice.name] to you."))
	//log_admin("[src] brought [choice] to their location.")

/client/verb/smite_living()

	set name = "Smite Living"
	set category = "Admin"

	var/list/valid_targets = list()

	for(var/k in all_players)
		valid_targets += k

	for(var/mob/living/L in view(src.mob,VIEW_RANGE))
		valid_targets |= L

	var/mob/living/L = input("What do you wish to crush?","Crush Target") as null|anything in valid_targets

	if(!L) return FALSE

	var/confirm = input("Are you sure you want to crush [L.name]? This will kill them instantly...","Cursh Confirmation","Cancel") as null|anything in list("Yes","Cancel")

	if(confirm != "Yes") return FALSE

	//var/turf/T = get_turf(L)
	//play('sound/meme/cbt.ogg',T)
	CALLBACK("\ref[L]_smite",15,L,/mob/living/proc/smite)

	//log_admin("[L.get_debug_name()] was smited by [src.get_debug_name()].")

obj/effect/temp/fist
	name = "FIST"
	icon = 'icons/obj/effects/fist_small.dmi'
	icon_state = "fist"
	desc = "Rip."
	duration = 10
	pixel_x = -TILE_SIZE*0.5

obj/effect/temp/fist/New(var/desired_location,var/desired_time,var/desired_color)
	. = ..()
	pixel_z = TILE_SIZE*2
	animate(src,pixel_z=0,time=desired_time*0.25)
	return .

/mob/living/proc/smite()
	var/turf/T = get_turf(src)
	for(var/mob/M in range(T,8))
		if(!M.client)
			continue
		M.client.queued_shakes += 5
	new/obj/effect/temp/fist(T,4,"#FFFFFF")
	play('sound/effects/anima_fragment_attack.ogg',T)
	on_crush()