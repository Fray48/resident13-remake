/client/verb/ghost()
	set name = "Ghost"
	set desc = "Abandon your body and become a ghost."
	set category = "Game"

	if(is_player(mob))
		var/mob/living/advanced/player/P = mob
		if(P.allow_save)
			var/obj/structure/interactive/bed/sleeper/S = locate() in P.loc.contents
			var/in_sleeper = istype(S,/obj/structure/interactive/bed/sleeper/backup) || istype(S,/obj/structure/interactive/bed/sleeper/cryo)
			if(in_sleeper)
				var/choice = input("Are you sure you want to save your character and cryo? You will no longer be able to be rejoin the round as this character.","Cryogenics","No") in list("Yes","No") | null
				if(choice == "Yes")
					var/savedata/client/mob/mobdata = MOBDATA(ckey)
					if(mobdata)
						mobdata.save_character(P)
					qdel(P)
					make_ghost(S.loc)
					return TRUE
				return FALSE

	if(permissions & FLAG_PERMISSION_MODERATOR)
		to_chat("You abandon your body as an admin")
		make_ghost(mob.loc)
		verbs += /client/verb/return_body
		return TRUE

	if(!mob || !is_living(mob))
		to_chat("You have no body to abandon!")
		return FALSE

	var/mob/living/L = mob

	if(!L.dead)
		to_chat("You can't abandon your body while alive!")
		return FALSE

	var/choice = input("Are you sure you want to abandon your body and become a ghost?","Abandon Body","No") in list("Yes","No") | null
	if(choice == "Yes")
		make_ghost(mob.loc)
		verbs += /client/verb/return_body
		to_chat("You abandon your body...")
		return TRUE


	return FALSE

/client/verb/return_body()
	set name = "Return body"
	//"Returns your body back to you. From void back to life."
	set category = "Game"

	var/confirm = input("Do you want to return back and try to fight for your life one more time?","Ashes or life") as null|anything in list("Grasp","Remain immortal")

	if(!confirm) return FALSE

	if(confirm == "Grasp")
		//all_clients[src.ckey] = src //способ сломать игрока
		var/mob/M
		for(M in all_players)
			if(!is_player(M))
				src.to_chat("[M.name]. /client/verb/return_body(). Please, show this message to devs.")
				continue
			if(mob.ckey_last == src.ckey)
				var/client/C = src
				C.control_mob(M)
				break
	else 
		to_chat("You remain unable to die")
		return FALSE

/client/verb/stop_sound()
	set name = "Stop Sounds"
	set category = "Game"
	mob << sound(null)
	to_chat("All sounds have been stopped.")

/client/verb/adjust_nightvision()
	set name = "Adjust Nightvision"
	set category = "Game"

	if(!mob)
		return FALSE

	var/desired_nightvision = input("What is your desired lighting alpha? (0 to 255)","Lighting Alpha",mob.lighting_mods["verb"] ? mob.lighting_mods["verb"] : 255) as null|num

	if(isnum(desired_nightvision))
		desired_nightvision = clamp(desired_nightvision,0,254)
		mob.add_lighting_mod("verb",desired_nightvision)
	else
		mob.remove_lighting_mod("verb")
