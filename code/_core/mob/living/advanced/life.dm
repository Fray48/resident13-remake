/mob/living/advanced/death_message()
	src.emote("deathgasp")
	return TRUE

/mob/living/advanced/on_life()
	. = ..()

	if(.)
		if(talk_duration)
			talk_duration = max(0,talk_duration-LIFE_TICK)
			if(talk_duration <= 0 && !is_typing)
				animate(chat_overlay,alpha = 0,time=SECONDS_TO_DECISECONDS(1))

	return .

/mob/living/advanced/on_life_slow()

	. = ..()

	if(.)
		//handle_regen() //Если реген не будет врубаться - это в помощь.
		handle_organs()	

	return .

/mob/living/advanced/pre_death()
	return TRUE

mob/living/advanced/revive()

	. = ..()

	for(var/k in overlays_assoc)
		var/image/overlay/O = overlays_assoc[k]
		O.plane = plane

	update_all_blends()

	return .

/mob/living/advanced/post_death()

	. = ..()

	drop_held_objects(src.loc)

	for(var/k in overlays_assoc)
		update_overlay_tracked(k, desired_plane = plane)

	return TRUE

/mob/living/proc/can_buffer_health()
	return (brute_regen_buffer || burn_regen_buffer || tox_regen_buffer || pain_regen_buffer || rad_regen_buffer) // || sanity_regen_buffer)

/mob/living/proc/can_buffer_stamina()
	return stamina_regen_buffer

/mob/living/proc/can_buffer_mana()
	return mana_regen_buffer

/mob/living/advanced/proc/handle_organs()

	if(!health)
		return FALSE

	for(var/k in labeled_organs)
		CHECK_TICK
		var/obj/item/organ/O = labeled_organs[k]
		if(O.has_life) O.on_life()

	return TRUE
