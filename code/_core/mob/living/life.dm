/mob/living/proc/death_message()
	src.visible_message("\The [src.name] seizes up and falls limp, their eyes dead and lifeless...")
	return TRUE

/mob/living/proc/death()

	if(dead)
		return FALSE

	death_message()

	pre_death()

	for(var/mob/living/advanced/player/P in view(VIEW_RANGE,src.loc))
		if(P == src)
			continue
		P.to_chat(span("notice","<b>\The [src.name] dies!</b>"),CHAT_TYPE_COMBAT)

	src.to_chat(span("danger","<h1>You died!</h1>"),CHAT_TYPE_COMBAT)
	src.to_chat(span("danger","Your death is not the end. Someone may come along and revive you, or maybe something will happen"))
	src.to_chat(span("danger","Be warned, if you choose to be ghosted there will be no way back!"))

	dead = TRUE
	time_of_death = world.time

	if(stand && stand.linked_stand)
		stand.linked_stand.set_enabled(FALSE)
		remove_stand()

	if(ai)
		ai.enabled = FALSE

	movement_flags = 0x0
	attack_flags = 0x0

	plane = PLANE_OBJ

	handle_horizontal()

	post_death()

	if(queue_delete_on_death)
		queue_delete(src,ITEM_DELETION_TIME_DROPPED,TRUE)

	return TRUE

/*
/mob/living/proc/do_loot_drop(var/atom/desired_loc)

	if(desired_loc && loot_drop && health)
		var/loot/L = all_loot[loot_drop]

		if(!isturf(desired_loc))
			return FALSE

		if(loot_drop_in_corpse)
			L.spawn_loot_corpse(desired_loc)
		else
			L.spawn_loot_turf(desired_loc)

		var/obj/item/currency/C = new(src.loc)
		C.value = 1 + FLOOR(health.health_max/10, 1)
		INITIALIZE(C)
		step_rand(C)
		return TRUE

	return FALSE
*/

/mob/living/proc/revive()
	movement_flags = 0x0
	attack_flags = 0x0
	dead = FALSE
	remove_status_effect(CRIT)
	plane = initial(plane)
	for(var/obj/hud/button/teleport_to_player/DG in buttons)
		DG.update_owner(null)
	if(health)
		health.update_health(update_hud=TRUE)
	handle_horizontal()
	undelete(src)
	return TRUE

/mob/living/proc/rejuvenate()
	if(health) health.adjust_loss_smart(-health.get_brute_loss(),-health.get_burn_loss(),-health.get_tox_loss(),-health.get_oxy_loss())
	if(reagents) reagents.remove_all_reagents()
	return TRUE

/mob/living/proc/resurrect()
	rejuvenate()
	revive()
	return TRUE

/mob/living/proc/pre_death()
	alert_overlay.icon_state = "none"
	chat_overlay.icon_state = "none"
	return TRUE

/mob/living/proc/post_death()
	HOOK_CALL("post_death")
	return TRUE

/mob/living/can_attack(var/atom/victim,var/atom/weapon,var/params,var/damagetype/damage_type)

	if(dead)
		return FALSE

	if(has_status_effect(list(PARALYZE,SLEEP,STAGGER,FATIGUE,STUN)))
		return FALSE

	return ..()


/mob/living/can_use_controls(object,location,control,params)

	if(dead)
		return FALSE

	if(has_status_effect(list(PARALYZE,SLEEP,STAGGER,STUN)))
		return FALSE

	return ..()

/*
/mob/living/proc/check_status_effects()

	//Crit
	if(!(status & CRIT) && (crit_time > 0 || crit_time == -1))
		add_status(CRIT)
		on_crit()

	if(status & CRIT && crit_time <= 0 && crit_time != -1)
		remove_status_effect(CRIT)
		on_uncrit()

	//Fatigue
	if(!(status & FATIGUE) && (fatigue_time > 0 || fatigue_time == -1))
		add_status(FATIGUE)
		on_fatigued()

	if(status & FATIGUE && fatigue_time <= 0 && fatigue_time != -1)
		remove_status_effect(FATIGUE)
		on_unfatigued()

	//Rest
	if(!(status & REST) && (rest_time > 0 || rest_time == -1))
		add_status(REST)
		on_rest()

	if(status & REST && rest_time <= 0 && rest_time != -1)
		remove_status_effect(REST)
		on_unrest()

	//Sleep
	if(!(status & SLEEP) && (sleep_time > 0 || sleep_time == -1))
		add_status(SLEEP)
		on_sleeped()

	if(status & SLEEP && sleep_time <= 0 && sleep_time != -1)
		remove_status_effect(SLEEP)
		on_unsleeped()

	//Stun
	if(!(status & STUN) && (stun_time > 0 || stun_time == -1))
		add_status(STUN)
		on_stunned()

	if(status & STUN && stun_time <= 0 && stun_time != -1)
		remove_status_effect(STUN)
		on_unstunned()

	//Stagger
	if(!(status & STAGGER) && (stagger_time > 0 || stagger_time == -1))
		add_status(STAGGER)
		on_staggered()

	if(status & STAGGER && stagger_time <= 0 && stagger_time != -1)
		remove_status_effect(STAGGER)
		on_unstaggered()

	//Paralyze
	if(!(status & PARALYZE) && (paralyze_time > 0 || paralyze_time == -1))
		add_status(PARALYZE)
		on_paralyzed()

	if(status & PARALYZE && paralyze_time <= 0 && paralyze_time != -1)
		remove_status_effect(PARALYZE)
		on_unparalyzed()

	//Confuse
	if(!(status & CONFUSED) && (confuse_time > 0 || confuse_time == -1))
		add_status(CONFUSED)
		on_confused()

	if(status & CONFUSED && confuse_time <= 0 && confuse_time != -1)
		remove_status_effect(CONFUSED)
		on_unconfused()

	//Adrenaline
	if(!(status & ADRENALINE) && (adrenaline_time > 0 || adrenaline_time == -1))
		add_status(ADRENALINE)
		on_adrenaline()

	if(status & ADRENALINE && adrenaline_time <= 0 && adrenaline_time != -1)
		remove_status_effect(ADRENALINE)
		on_unadrenaline()

	//Final Checks
	if(status && !(src in all_living_with_status))
		all_living_with_status += src

	if(!status && (src in all_living_with_status))
		handle_horizontal()
		all_living_with_status -= src

	return TRUE
*/

/mob/living/proc/handle_horizontal()

	var/desired_horizontal = dead || has_status_effect(list(STUN,FATIGUE,SLEEP,CRIT,REST))

	if(desired_horizontal != horizontal)
		if(desired_horizontal) //KNOCK DOWN
			animate(src,transform = turn(matrix(), stun_angle), pixel_z = 0, time = 1)
			update_collisions(FLAG_COLLISION_CRAWLING)
			play(pick('sound/effects/impacts/bodyfall2.ogg','sound/effects/impacts/bodyfall3.ogg','sound/effects/impacts/bodyfall4.ogg'),get_turf(src), volume = 25)
		else //GET UP
			animate(src,transform = matrix(), pixel_z = initial(src.pixel_z), time = 2)
			update_collisions(initial(collision_flags))
		horizontal = desired_horizontal

	return desired_horizontal

/mob/living/proc/on_life()

	if(!initialized || qdeleting)
		return FALSE

	handle_status_effects()

	handle_blocking()

	handle_health_buffer()

	update_alpha(handle_alpha())

	if(health && queue_health_update)
		health.update_health(update_hud = TRUE)
		queue_health_update = FALSE
	return TRUE

/mob/living/proc/handle_hunger()

	var/thirst_mod = health && (health.stamina_current <= health.stamina_max*0.5) ? 2 : 1
	var/quality_mod = 1 + clamp(1,0,1)*5

	add_nutrition(-(LIFE_TICK_SLOW/10)*0.05*quality_mod)
	add_nutrition_fast(-(LIFE_TICK_SLOW/10)*0.15*quality_mod)
	add_hydration(-(LIFE_TICK_SLOW/10)*0.08*thirst_mod)

	if(client)
		for(var/obj/hud/button/hunger/B in buttons)
			B.update_sprite()

	return TRUE

/mob/living/proc/on_life_slow()

	if(!initialized)
		return FALSE

	handle_fire()

	if(dead)
		return FALSE

	if(reagents)
		reagents.metabolize(src)

	handle_regen()

	if(blood_volume < blood_volume_max)
		var/consume_multiplier = 1
		var/blood_volume_to_add = -(add_hydration(-0.05*consume_multiplier) + add_nutrition(-0.3*consume_multiplier))*0.5
		blood_volume = clamp(blood_volume + blood_volume_to_add,0,blood_volume_max)
		queue_health_update = TRUE
	else if(blood_volume > blood_volume_max)
		blood_volume--
		if(health && blood_volume >= blood_volume_max*1.05)
			health.adjust_loss_smart(tox=0.5)

	handle_hunger()
	handle_intoxication()
	return TRUE

/mob/living/proc/handle_intoxication()

	if(intoxication) intoxication = max(0,(intoxication*0.999) - 0.1)

	switch(intoxication)
		if(0 to 200)
			if(last_intoxication_message != 0)
				to_chat(span("notice","Твоё настроение слегка приподнято."))
				last_intoxication_message = 0
		if(200 to 400)
			if(last_intoxication_message != 1)
				to_chat(span("notice","Кажется, ты немного подвыпил."))
				last_intoxication_message = 1
		if(400 to 800)
			if(last_intoxication_message != 2)
				to_chat(span("warning","Ты пьян."))
				last_intoxication_message = 2
		if(800 to 1600)
			if(last_intoxication_message != 3)
				to_chat(span("danger","Пора бы отставить бутылку..."))
				last_intoxication_message = 3
		if(1600 to INFINITY)
			if(last_intoxication_message != 4)
				to_chat(span("danger","Ты ужран в стельку. Сейчас бы не помешало проспаться."))
				last_intoxication_message = 4

	return TRUE


/mob/living/proc/handle_alpha()

	var/base_alpha = initial(alpha)

	if(is_sneaking)
		var/desired_alpha = FLOOR(10 + (1-stealth_mod)*base_alpha*0.5, 1)
		if(horizontal)
			desired_alpha *= 0.5
		return desired_alpha

	return base_alpha


/mob/living/proc/handle_health_buffer()

	if(!health)
		return FALSE

	var/update_health = FALSE
	var/update_stamina = FALSE
	var/update_mana = FALSE

	if(can_buffer_health())
		var/brute_to_regen = clamp(brute_regen_buffer,HEALTH_REGEN_BUFFER_MIN,HEALTH_REGEN_BUFFER_MAX)
		var/burn_to_regen = clamp(burn_regen_buffer,HEALTH_REGEN_BUFFER_MIN,HEALTH_REGEN_BUFFER_MAX)
		var/tox_to_regen = clamp(tox_regen_buffer,HEALTH_REGEN_BUFFER_MIN,HEALTH_REGEN_BUFFER_MAX)
		//var/pain_to_regen = clamp(pain_regen_buffer,HEALTH_REGEN_BUFFER_MIN,HEALTH_REGEN_BUFFER_MAX)
		//var/rad_to_regen = clamp(rad_regen_buffer,HEALTH_REGEN_BUFFER_MIN,HEALTH_REGEN_BUFFER_MAX)
		//var/sanity_to_regen = clamp(sanity_regen_buffer,HEALTH_REGEN_BUFFER_MIN,HEALTH_REGEN_BUFFER_MAX)
		health.adjust_loss_smart(
			brute = -brute_to_regen,
			burn = -burn_to_regen,
			tox=-tox_to_regen,
		)//rad=-rad_to_regen
		brute_regen_buffer -= brute_to_regen
		burn_regen_buffer -= burn_to_regen
		tox_regen_buffer -= tox_to_regen
		//pain_regen_buffer -= pain_to_regen
		//rad_regen_buffer -= rad_to_regen
		//sanity_regen_buffer -= sanity_to_regen
		update_health = TRUE

	if(can_buffer_stamina())
		var/stamina_to_regen = clamp(stamina_regen_buffer,STAMINA_REGEN_BUFFER_MIN,STAMINA_REGEN_BUFFER_MAX)
		health.adjust_stamina(stamina_to_regen)
		stamina_regen_buffer -= stamina_to_regen
		update_stamina = TRUE

	if(can_buffer_mana())
		var/mana_to_regen = clamp(mana_regen_buffer,MANA_REGEN_BUFFER_MIN,MANA_REGEN_BUFFER_MAX)
		health.adjust_mana(mana_to_regen)
		mana_regen_buffer -= mana_to_regen
		update_mana = TRUE

	if(update_health || update_stamina || update_mana)
		queue_health_update = TRUE
		return TRUE

	return FALSE
