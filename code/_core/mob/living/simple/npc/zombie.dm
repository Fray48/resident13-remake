/mob/living/simple/npc/zombie/
	iff_tag = "zombie"
	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/standart
	name = "Мамаша"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с сыном."
	icon = 'icons/mob/living/simple/voidmother.dmi'
	icon_state = "MatbVoida"
	damage_type = /damagetype/npc/zombie
	class = /class/zombie
	blood_color = "#1a1919"

	value = 25

	ai = /ai/zombie

	stun_angle = 0

	armor_base = list(
		BLADE = INFINITY,
		BLUNT = INFINITY,
		PIERCE = INFINITY,
		LASER = INFINITY,
		MAGIC = -50,
		HEAT = -20,
		COLD = INFINITY,
		BOMB = -50,
		BIO = INFINITY,
		RAD = INFINITY,
		HOLY = 50,
		DARK = 50,
		FATIGUE = 25
	)

	health_base = 250

	mob_size = MOB_SIZE_HUMAN

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

	movement_delay = DECISECONDS_TO_TICKS(2)

/mob/living/simple/npc/zombie/standart/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "standart_dead"
	update_sprite()

/mob/living/simple/npc/zombie/standart/army
	name = "Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_s.dmi'
	icon_state = "standart2"
	damage_type = /damagetype/npc/zombie
	class = /class/zombie

	armor_base = list(
		BLADE = INFINITY,
		BLUNT = INFINITY,
		PIERCE = INFINITY,
		LASER = INFINITY,
		MAGIC = -30,
		HEAT = -10,
		COLD = INFINITY,
		BOMB = -50,
		BIO = INFINITY,
		RAD = INFINITY,
		HOLY = 75,
		DARK = 60,
		FATIGUE = 25
	)

	health_base = 300

	movement_delay = DECISECONDS_TO_TICKS(3)

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/standart/army/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "standart_dead"
	update_sprite()
	if(prob(20))
		new/obj/item/clothing/head/helmet/security/old(src.loc)

/mob/living/simple/npc/zombie/standart/civ
	name = "Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_s.dmi'
	icon_state = "standart3"

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/standart/civ/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "standart_dead"
	update_sprite()
	if(prob(70))
		new/obj/item/clothing/uniform/res/destro_scientist(src.loc)
		new/obj/item/clothing/overwear/coat/res/civilian(src.loc)

/mob/living/simple/npc/zombie/standart/civ2
	name = "Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_s.dmi'
	icon_state = "standart4"

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/standart/civ2/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "standart_dead"
	update_sprite()

/mob/living/simple/npc/zombie/standart/civ3
	name = "Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_s.dmi'
	icon_state = "standart5"

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/standart/civ3/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "standart_dead"
	update_sprite()

/mob/living/simple/npc/zombie/standart/civ4
	name = "Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_s.dmi'
	icon_state = "standart6"

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/standart/civ4/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "standart_dead"
	update_sprite()

/mob/living/simple/npc/zombie/weak
	name = "Прокажённый Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_w.dmi'
	icon_state = "weak"
	damage_type = /damagetype/npc/zombie
	class = /class/zombie
	blood_color = "#1a1919"

	value = 25

	ai = /ai/zombie

	stun_angle = 0

	armor_base = list(
		BLADE = 50,
		BLUNT = 30,
		PIERCE = 60,
		LASER = INFINITY,
		MAGIC = -80,
		HEAT = -80,
		COLD = INFINITY,
		BOMB = -50,
		BIO = INFINITY,
		RAD = INFINITY,
		HOLY = 15,
		DARK = 10,
		FATIGUE = 25
	)

	health_base = 100

	mob_size = MOB_SIZE_HUMAN

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

	movement_delay = DECISECONDS_TO_TICKS(2)

/mob/living/simple/npc/zombie/weak/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "weak_dead"
	update_sprite()

/mob/living/simple/npc/zombie/weak/civ
	name = "Прокажённый Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_w.dmi'
	icon_state = "weak2"

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/weak/civ/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "[initial(icon_state)]_dead"
	update_sprite()

/mob/living/simple/npc/zombie/weak/civ2
	name = "Прокажённый Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_w.dmi'
	icon_state = "weak3"

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/weak/civ2/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "weak_dead"
	update_sprite()

/mob/living/simple/npc/zombie/weak/civ3
	name = "Прокажённый Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_w.dmi'
	icon_state = "weak4"

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/weak/civ3/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "weak_dead"
	update_sprite()

/mob/living/simple/npc/zombie/weak/unga
	name = "Прокажённый Тёмный"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_w.dmi'
	icon_state = "weak5"

	armor_base = list(
		BLADE = 70,
		BLUNT = 40,
		PIERCE = 70,
		LASER = INFINITY,
		MAGIC = -65,
		HEAT = -60,
		COLD = INFINITY,
		BOMB = -20,
		BIO = INFINITY,
		RAD = INFINITY,
		HOLY = 15,
		DARK = 10,
		FATIGUE = 25
	)

	health_base = 200

	movement_delay = DECISECONDS_TO_TICKS(3)

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

/mob/living/simple/npc/zombie/weak/unga/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "weak_dead"
	update_sprite()

/mob/living/simple/npc/zombie/heavy
	name = "Тёмный в тяжёлой броне"
	desc = "Когда-то оно было человеком"
	desc_extended = "Яркий пример того, как вирус повлиял на тех, кому не повезло с иммунитетом."
	icon = 'icons/mob/living/simple/zombie_h.dmi'
	icon_state = "heavy"
	damage_type = /damagetype/npc/zombie
	class = /class/zombie
	blood_color = "#1a1919"

	value = 25

	ai = /ai/zombie

	stun_angle = 0

	armor_base = list(
		BLADE = INFINITY,
		BLUNT = INFINITY,
		PIERCE = INFINITY,
		LASER = INFINITY,
		MAGIC = -50,
		HEAT = 10,
		COLD = INFINITY,
		BOMB = 30,
		BIO = INFINITY,
		RAD = INFINITY,
		HOLY = 70,
		DARK = 60,
		FATIGUE = 25
	)

	health_base = 350

	mob_size = MOB_SIZE_HUMAN

//	iff_tag = "zombie"
//	loyalty_tag = "zombie"

	movement_delay = DECISECONDS_TO_TICKS(3)

/mob/living/simple/npc/zombie/heavy/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "heavy_dead"
	update_sprite()

/mob/living/simple/npc/mook
	name = "Палач"
	desc = "Лучше не стоять у него на пути."
	desc_extended = "Огромный голем, собранный, будто бы, из десятка Тёмных поменьше."
	icon = 'icons/mob/living/simple/jungle/mook.dmi'
	icon_state = "mook"
	damage_type = /damagetype/npc/zombie
	class = /class/zombie

	value = 500

	ai = /ai/

	stun_angle = 0

	pixel_x = -16

	armor_base = list(
		BLADE = 60,
		BLUNT = 40,
		PIERCE = 55,
		LASER = 0,
		MAGIC = 25,
		HEAT = -40,
		COLD = 0,
		BOMB = -20,
		BIO = INFINITY,
		RAD = INFINITY,
		HOLY = -100,
		DARK = -50,
		FATIGUE = INFINITY
	)

	loyalty_tag = "zombie"
	iff_tag = "zombie"

	health_base = 600

	blood_color = "#262626"

	movement_delay = DECISECONDS_TO_TICKS(2)

	status_immune = list(
		STUN = TRUE,
		SLEEP = STAGGER,
		PARALYZE = STAGGER,
		FATIGUE = STAGGER,
		REST = FALSE,
		ADRENALINE = FALSE,
		DISARM = FALSE,
		DRUGGY = FALSE
	)

	mob_size = MOB_SIZE_GIANT

	enable_medical_hud = FALSE
	enable_security_hud = FALSE

/mob/living/simple/npc/mook/post_death()
	..()
	new/obj/effect/blood/splatter/oil(src.loc)
	icon_state = "mook_dead"
	update_sprite()
