/turf/simulated/wall/resident/beton
	name = "wall"
	icon = 'icons/stalker/beton_w_tg.dmi'
	icon_state = "wall"
	corner_icons = TRUE
	corner_category = "beton_w_tg"

/turf/simulated/wall/resident/wood
	name = "wall"
	icon = 'icons/walls.dmi'
	icon_state = "wall"
	corner_icons = TRUE
	corner_category = "walls"

/turf/simulated/wall/resident/wood1
	name = "wall"
	icon = 'icons/wood.dmi'
	icon_state = "wood0"
	corner_icons = TRUE
	corner_category = "wood"

/turf/simulated/wall/resident/polar
	name = "wall"
	icon = 'icons/stalker/PolarStation/walls.dmi'
	icon_state = "concrete0"
	corner_icons = TRUE
	corner_category = "walls"

/turf/simulated/wall/resident/metall
	name = "wall"
	icon = 'icons/stalker/wall.dmi'
	icon_state = "wall"
	corner_icons = TRUE
	corner_category = "wall"

/turf/simulated/wall/resident/metall2
	name = "wall"
	icon = 'icons/stalker/wall.dmi'
	icon_state = "wall"
	corner_icons = TRUE
	corner_category = "wall2"

/turf/simulated/wall/resident/canava
	name = "wall"
	icon = 'icons/wall.dmi'
	icon_state = "wall"
	corner_icons = TRUE
	corner_category = "wall"

/turf/simulated/wall/resident/void
	name = "end of world"
	icon = 'icons/turf/space/void.dmi'
	icon_state = "void"
	corner_icons = FALSE

/turf/simulated/floor/resident
	name = "re13 turf"
	icon = 'icons/stalker/grass.dmi'

/turf/simulated/floor/resident/floor
	name = "Grass"
	icon = 'icons/ground.dmi'
	icon_state = "grass1"

/turf/simulated/floor/resident/floor/grass
	real_icon = 'icons/turf/floor/grass_re.dmi'
	real_icon_state = "floor"
	corner_icons = TRUE
	corner_category = "grassre"

	footstep = /footstep/grass

/turf/simulated/floor/resident/floor/grass/dump
	icon = 'icons/stalker/zemlya.dmi'
	icon_state = "dump_grass1"

/turf/simulated/floor/resident/floor/pesok
	icon_state = "sand"

	footstep = /footstep/sand

/turf/simulated/floor/resident/floor/wood
	icon = 'icons/stalker/floor.dmi'
	name = "floor"

	footstep = /footstep/wood

/turf/simulated/floor/resident/floor/wood/brown
	icon_state = "wooden_floor"

/turf/simulated/floor/resident/floor/wood/grey
	icon_state = "wooden_floor2"

/turf/simulated/floor/resident/floor/wood/black
	icon_state = "wooden_floor3"

/turf/simulated/floor/resident/floor/wood/oldgor
	icon_state = "wood1"

/turf/simulated/floor/resident/floor/wood/oldvert
	icon_state = "woodd1"

/turf/simulated/floor/resident/floor/re13_old
	icon = 'icons/stalker/floor.dmi'
	icon_state = "bf25"
	name = "floor"

/turf/simulated/floor/resident/floor/re13_old/v2
	icon = 'icons/stalker/floor.dmi'
	icon_state = "F74"

/turf/simulated/floor/resident/floor/re13_old/v20
	icon = 'icons/floors.dmi'
	icon_state = "plating1"

/turf/simulated/floor/resident/floor/re13_old/v3
	icon = 'icons/stalker/floor.dmi'
	icon_state = "23323"

/turf/simulated/floor/resident/floor/re13_old/v4
	icon = 'icons/stalker/floor.dmi'
	icon_state = "snf23"

/turf/simulated/floor/resident/floor/re13_old/v5
	icon = 'icons/stalker/floor.dmi'
	icon_state = "nf24"

/turf/simulated/floor/resident/floor/re13_old/v6
	icon = 'icons/stalker/floor.dmi'
	icon_state = "1"

/turf/simulated/floor/resident/floor/re13_old/v7
	icon = 'icons/stalker/floor.dmi'
	icon_state = "surgery"

/turf/simulated/floor/resident/floor/re13_old/v8
	icon = 'icons/stalker/floor.dmi'
	icon_state = "npolar2"

/turf/simulated/floor/resident/floor/re13_old/v9
	icon = 'icons/stalker/floor.dmi'
	icon_state = "wtf"

/turf/simulated/floor/resident/floor/re13_old/v10
	icon = 'icons/stalker/floor.dmi'
	icon_state = "npolar"

/turf/simulated/floor/resident/floor/re13_old/v11
	icon = 'icons/stalker/floor.dmi'
	icon_state = "nfloorz"

/turf/simulated/floor/resident/floor/re13_old/open
	icon = 'icons/stalker/floor.dmi'
	icon_state = "openspace"

/turf/simulated/floor/resident/floor/re13_old/v12
	icon = 'icons/stalker/metro-2/Rails.dmi'
	icon_state = "rail_straight"

/turf/simulated/floor/resident/floor/re13_old/v13
	icon = 'icons/stalker/metro-2/railroads.dmi'
	icon_state = "railroad"

/turf/simulated/floor/resident/floor/re13_old/v18
	icon = 'icons/stalker/lohweb/floors.dmi'
	icon_state = "bfloorz"

/turf/simulated/floor/resident/floor/re13_old/v19
	icon = 'icons/stalker/lohweb/floors.dmi'
	icon_state = "polar"

/turf/simulated/floor/resident/floor/asphalt
	name = "road"
	icon = 'icons/stalker/Prishtina/asphalt.dmi'
	icon_state = "road1"

/turf/simulated/floor/resident/floor/asphalt/New()
	icon_state = "road[rand(1, 3)]"

/turf/simulated/floor/resident/floor/cement
	name = "asphalt"
	icon = 'icons/stalker/building_road.dmi'
	icon_state = "road1"
	footstep = /footstep/concrete

/turf/simulated/floor/resident/floor/cement/New()
	icon_state = "road[rand(1, 2)]"

/turf/simulated/floor/resident/floor/gryaz
	name = "dirt"
	icon = 'icons/ground.dmi'
	icon_state = "dirt"

	real_icon = 'icons/turf/floor/dirt_re.dmi'
	real_icon_state = "floor"
	corner_icons = TRUE
	corner_category = "dirtre"

/*var/global/list/GryazEdgeCache

/turf/simulated/floor/resident/floor/gryaz/New()
	if(!GryazEdgeCache || !GryazEdgeCache.len)
		GryazEdgeCache = list()
		GryazEdgeCache.len = 10
		GryazEdgeCache[NORTH] = image('icons/stalker/tropa.dmi', "tropa_side_n", layer = 2.01)
		GryazEdgeCache[SOUTH] = image('icons/stalker/tropa.dmi', "tropa_side_s", layer = 2.01)
		GryazEdgeCache[EAST] = image('icons/stalker/tropa.dmi', "tropa_side_e", layer = 2.01)
		GryazEdgeCache[WEST] = image('icons/stalker/tropa.dmi', "tropa_side_w", layer = 2.01)

	spawn(1)
		var/turf/T
		for(var/i = 0, i <= 3, i++)
			if(!get_step(src, 2**i))
				continue
			if(overlay_priority > get_step(src, 2**i).overlay_priority)
				T = get_step(src, 2**i)
				if(T)
					T.overlays += GryazEdgeCache[2**i]
	return*/

/obj/structure/vehicle/ural
	name = "Truck"
	plane = -9
	icon = 'ural.dmi'

/obj/structure/vehicle/ural/wall
	collision_flags = FLAG_COLLISION_WALL

/obj/structure/vehicle/ural/wall/one
	icon_state = "1"
/obj/structure/vehicle/ural/wall/two
	icon_state = "2"
/obj/structure/vehicle/ural/wall/three
	icon_state = "3"
/obj/structure/vehicle/ural/wall/four
	icon_state = "4"
/obj/structure/vehicle/ural/wall/nine
	icon_state = "9"
/obj/structure/vehicle/ural/wall/ten
	icon_state = "10"
/obj/structure/vehicle/ural/wall/eleven
	icon_state = "11"
/obj/structure/vehicle/ural/wall/twelve
	icon_state = "12"
/obj/structure/vehicle/ural/wall/thirteen
	opacity = 1
	icon_state = "13"
/obj/structure/vehicle/ural/wall/sixteen
	opacity = 1
	icon_state = "16"
/obj/structure/vehicle/ural/wall/seventeen
	opacity = 1
	icon_state = "17"
/obj/structure/vehicle/ural/wall/eightteen
	opacity = 1
	icon_state = "18"

/obj/structure/vehicle/ural/floor

/obj/structure/vehicle/ural/floor/five
	icon_state = "5"
/obj/structure/vehicle/ural/floor/six
	icon_state = "6"
/obj/structure/vehicle/ural/floor/seven
	icon_state = "7"
/obj/structure/vehicle/ural/floor/eight
	icon_state = "8"
/obj/structure/vehicle/ural/floor/fourteen
	icon_state = "14"
/obj/structure/vehicle/ural/floor/fifthteen
	icon_state = "15"
/obj/structure/vehicle/ural/floor/wood_l
	icon_state = "wood_l"
/obj/structure/vehicle/ural/floor/wood_r
	icon_state = "wood_r"

/obj/structure/vehicle/ural/driver
	icon_state = "panel"

/turf/simulated/floor/resident_new
	icon = 'floors.dmi'
	icon_state = "plating"
	name = "floor"

	footstep = /footstep/tile

/turf/simulated/floor/resident_new/moving
	icon = 'floors.dmi'
	icon_state = "plating_moving"
	name = "floor"

	footstep = /footstep/tile

/turf/simulated/floor/resident_new/tiled
	icon_state = "floor"

/turf/simulated/floor/resident_new/tiled/dark
	icon_state = "floor_dark"

/turf/simulated/floor/resident_new/tiled/white
	icon_state = "floor_white"

/turf/simulated/floor/resident_new/tiled/dirty
	icon_state = "floor_dirty"

/turf/simulated/floor/resident_new/tiled/freezer
	icon_state = "floor_freezer"

/turf/simulated/floor/resident_new/tiled/tech
	icon_state = "floor_tech"

/turf/simulated/floor/resident_new/tiled/techvent
	icon_state = "floor_techvent"

/turf/simulated/floor/resident_new/ramp
	icon_state = "ramp"

/turf/simulated/floor/resident_new/lino
	icon_state = "lino1"

/turf/simulated/floor/resident_new/sidewalk
	icon_state = "pavement"

/turf/simulated/floor/resident_new/lino/alt
	icon_state = "lino2"

/turf/simulated/floor/resident_new/wood
	icon_state = "wood1"

/turf/simulated/floor/resident_new/wood/alt
	icon_state = "wood2"

/turf/simulated/floor/resident_new/wood/alt2
	icon_state = "wood3"

/turf/simulated/floor/resident_new/wood/alt3
	icon_state = "wood4"

/turf/simulated/floor/resident_new/wood/alt4
	icon_state = "wood5"

/turf/simulated/floor/resident_new/wood/alt5
	icon_state = "wood6"

/turf/simulated/floor/resident_new/wood/alt6
	icon_state = "wood7"

/turf/simulated/floor/resident_new/boat
	icon = 'boat.dmi'
	icon_state = "2"