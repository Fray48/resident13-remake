/damagetype/melee/club/bat/
	name = "pickaxe"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BLADE = 5,
		BLUNT = 15
	)

	//How much armor to penetrate. It basically removes the percentage of the armor using these values.
	attack_damage_penetration = list(
		BLUNT = 5
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 5,
		ATTRIBUTE_DEXTERITY = 5
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = BLUNT,
		ATTRIBUTE_DEXTERITY = BLUNT
	)

	skill_stats = list(
		SKILL_MELEE = 5
	)

	skill_damage = list(
		SKILL_MELEE = BLUNT
	)

/damagetype/melee/club/bat_upgraded
	name = "pickaxe"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BLADE = 5,
		BLUNT = 25
	)

	//How much armor to penetrate. It basically removes the percentage of the armor using these values.
	attack_damage_penetration = list(
		BLUNT = 10
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 5,
		ATTRIBUTE_DEXTERITY = 5
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = BLUNT,
		ATTRIBUTE_DEXTERITY = BLUNT
	)

	skill_stats = list(
		SKILL_MELEE = 5
	)

	skill_damage = list(
		SKILL_MELEE = BLUNT
	)

/damagetype/melee/club/bat_upgraded2
	name = "pickaxe"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BLADE = 15,
		BLUNT = 20
	)

	//How much armor to penetrate. It basically removes the percentage of the armor using these values.
	attack_damage_penetration = list(
		BLUNT = 10
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 5,
		ATTRIBUTE_DEXTERITY = 5
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = BLUNT,
		ATTRIBUTE_DEXTERITY = BLUNT
	)

	skill_stats = list(
		SKILL_MELEE = 5
	)

	skill_damage = list(
		SKILL_MELEE = BLUNT
	)

/damagetype/melee/club/bat_upgraded3
	name = "pickaxe"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BLADE = 35,
		BLUNT = 15
	)

	//How much armor to penetrate. It basically removes the percentage of the armor using these values.
	attack_damage_penetration = list(
		BLADE = 15,
		BLUNT = 5
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 5,
		ATTRIBUTE_DEXTERITY = 5
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = BLUNT,
		ATTRIBUTE_DEXTERITY = BLUNT
	)

	skill_stats = list(
		SKILL_MELEE = 10
	)

	skill_damage = list(
		SKILL_MELEE = BLUNT
	)