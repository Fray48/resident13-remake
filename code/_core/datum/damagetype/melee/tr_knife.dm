/damagetype/melee/tr_knife/
	name = "spear"

	attack_damage_base = list(
		PIERCE = 10,
		BLADE = 40
	)

	attack_damage_penetration = list(
		PIERCE = 20,
		BLADE = 35
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 20,
		ATTRIBUTE_DEXTERITY = 10
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = BLADE
	)

	skill_stats = list(
		SKILL_MELEE = 15
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE
	)

/damagetype/melee/tr_knife/thrown

	name = "thrown spear"

	attack_damage_base = list(
		PIERCE = 30,
		BLADE = 70
	)

	attack_damage_penetration = list(
		PIERCE = 70,
		BLADE = 70
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 20,
		ATTRIBUTE_DEXTERITY = 10
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = PIERCE
	)

	skill_stats = list(
		SKILL_MELEE = 10,
		SKILL_RANGED = 15
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE,
		SKILL_RANGED = PIERCE
	)