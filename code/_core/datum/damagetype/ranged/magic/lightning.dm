/damagetype/ranged/magic/lightning
	name = "lightning"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BURN = 100,
		MAGIC = 80
	)

	attribute_stats = list(
		ATTRIBUTE_INTELLIGENCE = 15,
		ATTRIBUTE_WILLPOWER = 10
	)

	attribute_damage = list(
		ATTRIBUTE_INTELLIGENCE = MAGIC
	)

	skill_stats = list(
		SKILL_PRAYER = 10,
		SKILL_MAGIC = 10
	)

	skill_damage = list(
		SKILL_PRAYER = HOLY
	)