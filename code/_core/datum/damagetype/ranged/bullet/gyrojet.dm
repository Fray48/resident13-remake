/damagetype/ranged/bullet/gyrojet/
	name = "Gyrojet Impact"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BLUNT = 10,
		PIERCE = 10,
	)

	//How much armor to penetrate. It basically removes the percentage of the armor using these values.
	attack_damage_penetration = list(
		BLADE = 25,
		PIERCE = 25,
	)

	falloff = 0

/damagetype/ranged/bullet/gl/
	name = "Gyrojet Impact"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BLUNT = 30,
		PIERCE = 20,
		HEAT = 70
	)

	//How much armor to penetrate. It basically removes the percentage of the armor using these values.
	attack_damage_penetration = list(
		BLADE = 40,
		PIERCE = 25,
		HEAT = 50
	)

	falloff = 0