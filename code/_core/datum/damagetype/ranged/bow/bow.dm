/damagetype/ranged/bow/
	name = "bow"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BLUNT = 30,
		PIERCE = 60
	)

	//How much armor to penetrate. It basically removes the percentage of the armor using these values.
	attack_damage_penetration = list(
		BLUNT = 35,
		PIERCE = 60
	)

	falloff = VIEW_RANGE*2