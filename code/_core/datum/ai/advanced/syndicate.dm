/ai/advanced/syndicate
	enemy_tags = list("NanoTrasen","Skeleton")
	should_find_weapon = TRUE
	debug = TRUE


/ai/advanced/syndicate/on_damage_received(var/atom/atom_damaged,var/atom/attacker,var/atom/weapon,var/list/damage_table,var/damage_amount,var/stealthy=FALSE)

	. = ..()

	if(damage_amount >= 10 && . && prob(25))
		if(prob(10) && get_dist(owner,attacker) >= 3)
			var/attack_dir = dir2text(get_dir(owner,attacker))
			owner.say("Taking fire from the [attack_dir]!")
		else
			var/list/responses = list(
				"Ранен!",
				"Я под огнём!",
				"Они достали меня!",
				"Ранен в [atom_damaged.name]!",
				"Я под обстрелом, прикройте!",
				"Блять! Я ранен!"
			)
			owner.say(pick(responses))

	return .


/ai/advanced/syndicate/on_alert_level_changed(var/old_alert_level,var/new_alert_level,var/atom/alert_source)

	. = ..()

	if(. && prob(25))
		var/list/responses = list()
		if(old_alert_level == ALERT_LEVEL_COMBAT && new_alert_level == ALERT_LEVEL_CAUTION)
			responses = list(
				"Не вижу их...",
				"Потерял из виду.",
				"Никого не видно.",
				"Они точно тут?"
			)
		else if(old_alert_level == ALERT_LEVEL_COMBAT && new_alert_level == ALERT_LEVEL_NONE)
			responses = list(
				"Кажись готов",
				"Очередной выскочка.",
				"Бежал сверкая пятками, ха-ха!"
			)
		else if(old_alert_level == ALERT_LEVEL_CAUTION && new_alert_level == ALERT_LEVEL_COMBAT)
			responses = list(
				"Нашёл тебя!",
				"Я знал, что мне не показалось!",
				"Противник подтверждён!",
				"Противник обнаружен!"
			)
		else if(old_alert_level == ALERT_LEVEL_NONE && new_alert_level == ALERT_LEVEL_NOISE)
			responses = list(
				"Ты слышал?",
				"Что это было?",
				"Ты что-нибудь слышал?",
				"Стой. Что это было?"
			)
		else if(new_alert_level == ALERT_LEVEL_NOISE)
			responses = list(
				"Я точно что-то слышал...",
				"Где ты?",
				"Вылазь... где же ты?",
				"Я клянусь что слышал что-то..."
			)
		else if(new_alert_level == ALERT_LEVEL_NONE)
			responses = list(
				"Крысы наверное.",
				"Возвращаюсь в патруль."
			)

		if(length(responses))
			owner.say(pick(responses))

	return .