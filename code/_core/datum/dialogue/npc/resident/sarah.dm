/dialogue/npc/resident/quest/sarah

/dialogue/npc/resident/quest/sarah/get_dialogue_options(var/mob/living/advanced/player/P,var/list/known_options)

	. = ..()

	. = ..()

	.["hello"] = list(
		"Не могу поверить, что он больше не с нами...",
		"*подожди тут",
		"*следуй за мной",
		"Прошлое"
	)

	.["*подожди тут"] = list(
		"Жду."
	)

	.["*следуй за мной"] = list(
		"Мы уходим?...Хорошо."
	)

	.["Прошлое"] = list(
		"Оно тебе так итересно?..."
	)

	.["Томми"] = list(
		"Он был хорошим другом, даже слишком. Жаль, что его судьба обернулась именно так..."
	)

/dialogue/npc/resident/quest/sarah/set_topic(var/mob/living/advanced/player/P,var/topic)

	. = ..()

	if(!is_living(P.dialogue_target))
		return .

	var/mob/living/L = P.dialogue_target

	if(!L.ai)
		return .

	switch(topic)
		if("*подожди тут")
			L.ai.set_move_objective(null)
		if("*следуй за мной")
			L.ai.set_move_objective(P,TRUE)

	return .