/mob/living/vehicle/moped
	name = "Vehicle"
	desc = "A large vehicle that holds a driver"
	icon = 'icons/obj/vehicles/motor.dmi'
	icon_state = "moped"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	opacity = 0
	anchored = 0

	movement_delay = DECISECONDS_TO_TICKS(1)

	layer = LAYER_MOB_BELOW
	health_base = 800

	buttons_to_add = list(
		/obj/hud/button/vehicle/eject
	)