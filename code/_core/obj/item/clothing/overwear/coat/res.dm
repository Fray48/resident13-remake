/obj/item/clothing/overwear/coat/res/science
	name = "синяя куртка с логтипом D.E.S.T.R.O."
	desc = "НАУКА РУЛИТ!"
	desc_extended = "Карманы в комплекте."
	icon = 'icons/obj/item/clothing/suit/destro_science.dmi'

	size = SIZE_2*2
	dynamic_inventory_count = 2
	container_max_size = SIZE_2

	defense_rating = list(
		LASER = -25,
		MAGIC = -50,
		HEAT = 30,
		COLD = 35,
		BIO = 40,
	)

	weight = WEIGHT_2

	value = 50

/obj/item/clothing/overwear/coat/res/science/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	if(is_inventory(object))
		click_self(caller,location,control,params)
		return TRUE

	return ..()

/obj/item/clothing/overwear/coat/res/civilian
	name = "Серое пальто"
	desc = "Такое же заебавшееся, как всё вокруг"
	desc_extended = "Пережиток прошлого, с парой встроенных металлических пластин и карманами. Не лучшая защита - но уже что-то."
	icon = 'icons/obj/item/clothing/suit/palto.dmi'

	size = SIZE_2*2
	dynamic_inventory_count = 2
	container_max_size = SIZE_2

	defense_rating = list(
		BLUNT = 20,
		BLADE = 15,
		PIERCE = 10,
		LASER = -25,
		MAGIC = -50,
		HEAT = 30,
		COLD = 35,
		BIO = 40,
	)

	weight = WEIGHT_2

	value = 50

/obj/item/clothing/overwear/coat/res/civilian/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	if(is_inventory(object))
		click_self(caller,location,control,params)
		return TRUE

	return ..()

/obj/item/clothing/overwear/coat/res/gorka
	name = "Горка"
	desc = "Старая, немного потрёпанная горка"
	desc_extended = "Просто красивая куртка."
	icon = 'icons/obj/item/clothing/suit/gorka.dmi'

	size = SIZE_2
	additional_clothing = list(/obj/item/clothing/head/hood/gorka)

	defense_rating = list(
		BLUNT = 10,
		BLADE = 10,
		PIERCE = 5,
		LASER = -25,
		MAGIC = -50,
		HEAT = 40,
		COLD = 40,
		BIO = 20,
	)

	weight = WEIGHT_2

	value = 50

/obj/item/clothing/head/hood/gorka
	name = "капюшон"
	desc = "Защищает голову, когда под рукой нет чем ещё прикрыться."
	desc_extended = "Защищает голову от ветра, дождя, и прочих погодных условий, когда под рукой нет шапки."
	icon = 'icons/obj/item/clothing/hats/gorka_hood.dmi'
//	item_slot = SLOT_HEAD | SLOT_HEAD_A

	hidden_organs = list(
		BODY_HAIR_HEAD = TRUE
	)

	defense_rating = list(
		LASER = -25,
		MAGIC = 50,
		HEAT = -25,
		COLD = 50
	)

	protection_cold = list(
		BODY_HEAD = 5,
	)

	size = SIZE_2
	weight = WEIGHT_1

	value = 10

/obj/item/clothing/overwear/coat/res/donor
	name = "Серое пальто"
	desc = "Такое же заебавшееся, как всё вокруг"
	desc_extended = "Пережиток прошлого, с парой встроенных металлических пластин и карманами. Такую часто использовали контрабандисты."
	icon = 'icons/obj/item/clothing/suit/civ1.dmi'

	size = SIZE_2*2
	dynamic_inventory_count = 2
	container_max_size = SIZE_2

	defense_rating = list(
		BLUNT = 20,
		BLADE = 15,
		PIERCE = 10,
		LASER = -25,
		MAGIC = -50,
		HEAT = 30,
		COLD = 35,
		BIO = 40,
	)

	weight = WEIGHT_2

	value = 50

/obj/item/clothing/overwear/coat/res/donor/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	if(is_inventory(object))
		click_self(caller,location,control,params)
		return TRUE

	return ..()

/obj/item/clothing/overwear/coat/res/donor2
	name = "Плащ-палатка"
	desc = "Лучший друг диверсанта"
	desc_extended = "Отлично греет. Ещё и как прикрытие использовать можно."
	icon = 'icons/obj/item/clothing/suit/palatka.dmi'

	size = SIZE_2
	additional_clothing = list(/obj/item/clothing/head/hood/donor)

	defense_rating = list(
		BLUNT = 10,
		BLADE = 10,
		PIERCE = 5,
		LASER = -25,
		MAGIC = -50,
		HEAT = 60,
		COLD = 60,
		BIO = 20,
	)

	weight = WEIGHT_2

	value = 50

/obj/item/clothing/head/hood/donor
	name = "капюшон"
	desc = "Защищает голову, когда под рукой нет чем ещё прикрыться."
	desc_extended = "Защищает голову от ветра, дождя, и прочих погодных условий, когда под рукой нет шапки."
	icon = 'icons/obj/item/clothing/hats/palatka.dmi'
//	item_slot = SLOT_HEAD | SLOT_HEAD_A

	hidden_organs = list(
		BODY_HAIR_HEAD = TRUE
	)

	defense_rating = list(
		LASER = -25,
		MAGIC = -25,
		HEAT = 60,
		COLD = 60
	)

	protection_cold = list(
		BODY_HEAD = 20,
	)

	size = SIZE_2
	weight = WEIGHT_1

	value = 10