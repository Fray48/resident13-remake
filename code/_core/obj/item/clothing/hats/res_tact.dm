/obj/item/clothing/head/res/tactical_helmet
	name = "tactical D.E.S.T.R.O. helmet"
	icon = 'icons/obj/item/clothing/hats/rehelm.dmi'
	desc = "Он прошёл через многое"
	desc_extended = "Достаточно потрёпанный тактический шлем. Его бывший владелец явно повидал многое."

	defense_rating = list(
		BLADE = 40,
		BLUNT = 80,
		PIERCE = 40,
		LASER = 30,
		MAGIC = -50,
		BOMB = 30
	)

	size = SIZE_3
	weight = WEIGHT_2

	value = 80