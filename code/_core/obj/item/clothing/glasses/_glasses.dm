/obj/item/clothing/glasses/
	worn_layer = LAYER_MOB_CLOTHING_EYE
	item_slot = SLOT_EYES

	protected_limbs = list(BODY_HEAD)

	size = SIZE_0
	weight = WEIGHT_0

	var/vision_mod = FLAG_VISION_NONE
	var/sight_mod = 0x0

	blocks_clothing = SLOT_FACE_WRAP

	var/see_invisible = 0
	var/see_in_dark = 0
	
/*/mob/living/advanced/update_eyes()

	. = ..()

	for(var/obj/item/organ/eye/E in organs)
		sight |= E.sight_mod
		vision |= E.vision_mod
		see_invisible = max(E.see_invisible,see_invisible)
		see_in_dark = max(see_in_dark,E.see_in_dark)

	for(var/obj/item/clothing/glasses/G in worn_objects)
		sight |= G.sight_mod
		vision |= G.vision_mod
		see_invisible = max(G.see_invisible,see_invisible)
		see_in_dark = max(see_in_dark,G.see_in_dark)
	blocks_clothing = SLOT_EYES | SLOT_FACE_WRAP*/