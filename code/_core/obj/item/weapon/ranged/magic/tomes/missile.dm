/obj/item/weapon/ranged/magic/tome/missile
	name = "strange device"
	desc = "MAGIC MISSILE! MAGIC MISSILE! CLOTHING DOESN'T COUNT!"
	desc_extended = "Fires a magical missile."
	cost_mana = 30
	shoot_delay = 5

	item_slot = SLOT_HAND_RIGHT | SLOT_HAND_LEFT
	worn_layer = LAYER_MOB_CLOTHING_BACK
	slot_icons = TRUE

	icon = 'icons/obj/item/weapons/ranged/magic/tomes/missile.dmi'

	projectile = /obj/projectile/magic/magic_missile

	ranged_damage_type = /damagetype/ranged/magic/magic_missile

	shoot_sounds = list('sound/weapons/magic/magic_missile.ogg')

	projectile_speed = 20

	value = 500

/obj/item/weapon/ranged/magic/tome/missile/can_be_worn(var/mob/living/advanced/owner,var/obj/hud/inventory/I)
	return TRUE