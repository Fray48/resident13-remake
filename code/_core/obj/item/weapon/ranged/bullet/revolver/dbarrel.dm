/obj/item/weapon/ranged/bullet/revolver/dbarrel
	name = "\improper 12 gauge boomstick"
	desc = "Двухзарядный дробовик."
	desc_extended = "Двухзарядный обрез для ведения боя на мелких дистанциях и молниеносного нанесения ударов."
	icon = 'icons/obj/item/weapons/ranged/dbarrel.dmi'
	icon_state = "inventory"
	crafting_id = "dbarrel"

	shoot_delay = 2

	automatic = FALSE

	bullet_count_max = 2

	insert_limit = 2

	view_punch = 12

	shoot_sounds = list('sound/weapons/combat_shotgun/shoot.ogg')

	slowdown_mul_held = HELD_SLOWDOWN_SHOTGUN_SMALL

	size = SIZE_2
	weight = WEIGHT_2

	bullet_length_min = 18
	bullet_length_best = 18.5
	bullet_length_max = 19

	bullet_diameter_min = 18
	bullet_diameter_best = 18.5
	bullet_diameter_max = 19

	heat_per_shot = 0.05
	heat_max = 0.1

	value = 60

/obj/item/weapon/ranged/bullet/revolver/dbarrel/get_base_spread()
	return 0.1

/obj/item/weapon/ranged/bullet/revolver/dbarrel/get_static_spread() //Base spread
	return 0.01

/obj/item/weapon/ranged/bullet/revolver/dbarrel/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.03 - (0.12 * L.get_skill_power(SKILL_RANGED)))

/obj/item/weapon/ranged/bullet/revolver/dbarrel/four
	name = "\improper 12 gauge boomstick"
	desc = "Четырёхзарядный дробовик"
	desc_extended = "Четырёхствольный обрез для ведения боя на мелких дистанциях и молниеносного нанесения ударов."
	icon = 'icons/obj/item/weapons/ranged/fbarrel.dmi'
	icon_state = "inventory"
	crafting_id = "fbarrel"

	shoot_delay = 3

	automatic = FALSE

	bullet_count_max = 4

	insert_limit = 4

	view_punch = 12

	shoot_sounds = list('sound/weapons/combat_shotgun/shoot.ogg')

	slowdown_mul_held = HELD_SLOWDOWN_SHOTGUN_SMALL

	size = SIZE_2
	weight = WEIGHT_3

	bullet_length_min = 18
	bullet_length_best = 18.5
	bullet_length_max = 19

	bullet_diameter_min = 18
	bullet_diameter_best = 18.5
	bullet_diameter_max = 19

	heat_per_shot = 0.03
	heat_max = 0.1

	value = 60

/obj/item/weapon/ranged/bullet/revolver/dbarrel/four/get_base_spread()
	return 0.2

/obj/item/weapon/ranged/bullet/revolver/dbarrel/four/get_static_spread() //Base spread
	return 0.02

/obj/item/weapon/ranged/bullet/revolver/dbarrel/four/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.04 - (0.13 * L.get_skill_power(SKILL_RANGED)))

/obj/item/weapon/ranged/bullet/revolver/dbarrel/six
	name = "\improper 12 gauge boomstick"
	desc = "Шестизарядный дробовик"
	desc_extended = "Шестиствольный револьвер-дробовик для ведения боя на мелких дистанциях и молниеносного нанесения ударов."
	icon = 'icons/obj/item/weapons/ranged/sbarrel.dmi'
	icon_state = "inventory"

	shoot_delay = 1

	automatic = FALSE

	bullet_count_max = 6

	insert_limit = 6

	view_punch = 12

	shoot_sounds = list('sound/weapons/combat_shotgun/shoot.ogg')

	slowdown_mul_held = HELD_SLOWDOWN_SHOTGUN_SMALL

	size = SIZE_5
	weight = WEIGHT_4

	bullet_length_min = 18
	bullet_length_best = 18.5
	bullet_length_max = 19

	bullet_diameter_min = 18
	bullet_diameter_best = 18.5
	bullet_diameter_max = 19

	heat_per_shot = 0.03
	heat_max = 0.2

	value = 60

/obj/item/weapon/ranged/bullet/revolver/dbarrel/six/get_base_spread()
	return 0.2

/obj/item/weapon/ranged/bullet/revolver/dbarrel/six/get_static_spread() //Base spread
	return 0.03

/obj/item/weapon/ranged/bullet/revolver/dbarrel/six/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.06 - (0.10 * L.get_skill_power(SKILL_RANGED)))

/obj/item/weapon/ranged/bullet/revolver/obarrel
	name = "\improper 12 gauge boomstick"
	desc = "Однозарядная бум-палка."
	desc_extended = "Однозарядная труба для ведения боя на мелких дистанциях и нанесения решающих ударов."
	icon = 'icons/obj/item/weapons/ranged/obarrel.dmi'
	icon_state = "inventory"

	shoot_delay = 3

	automatic = FALSE

	bullet_count_max = 1

	insert_limit = 1

	view_punch = 12

	shoot_sounds = list('sound/weapons/combat_shotgun/shoot.ogg')

	slowdown_mul_held = HELD_SLOWDOWN_SHOTGUN_SMALL

	size = SIZE_3
	weight = WEIGHT_3

	bullet_length_min = 18
	bullet_length_best = 18.5
	bullet_length_max = 19

	bullet_diameter_min = 18
	bullet_diameter_best = 18.5
	bullet_diameter_max = 19

	heat_per_shot = 0.05
	heat_max = 0.1

	value = 60

/obj/item/weapon/ranged/bullet/revolver/obarrel/get_base_spread()
	return 0.1

/obj/item/weapon/ranged/bullet/revolver/obarrel/get_static_spread() //Base spread
	return 0.01

/obj/item/weapon/ranged/bullet/revolver/dbarrel/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.03 - (0.12 * L.get_skill_power(SKILL_RANGED)))
