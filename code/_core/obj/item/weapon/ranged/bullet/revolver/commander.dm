/obj/item/weapon/ranged/bullet/revolver/commander
	name = "\improper Револьвер заводского производства"
	desc = "Старый, заводской револьвер."
	desc_extended = "Некая жгучая смесь двух миров - дробовиков и револьверов. Стреляет достаточно больно, чаще всего - одного выстрела хватает на одну цель. Расчитан под 44-ый калибр и имеет при себе барабан на 6 патрон."
	icon = 'icons/obj/item/weapons/ranged/revolver/44.dmi'
	icon_state = "inventory"
	crafting_id = "rev_stand"

	shoot_delay = 3

	automatic = FALSE

	bullet_count_max = 6

	shoot_sounds = list('sound/weapons/revolver_heavy/shoot.ogg')

	view_punch = 10

	size = SIZE_2
	weight = WEIGHT_3

	slowdown_mul_held = HELD_SLOWDOWN_REVOLVER

	bullet_length_min = 10
	bullet_length_best = 29
	bullet_length_max = 30

	bullet_diameter_min = 10
	bullet_diameter_best = 10.9
	bullet_diameter_max = 11

	value = 170

	heat_per_shot = 0.03
	heat_max = 0.1

/obj/item/weapon/ranged/bullet/revolver/commander/get_static_spread() //Base spread
	return 0

/obj/item/weapon/ranged/bullet/revolver/commander/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.05 - (0.1 * L.get_skill_power(SKILL_RANGED)))

/obj/item/weapon/ranged/bullet/revolver/commander/small
	name = "\improper Револьвер заводского производства"
	desc = "Старый, заводской револьвер."
	desc_extended = "Некая жгучая смесь двух миров - дробовиков и револьверов. Стреляет достаточно больно, чаще всего - одного выстрела хватает на одну цель. Расчитан под 44-ый калибр и имеет при себе барабан на 6 патрон."
	icon = 'icons/obj/item/weapons/ranged/revolver/44_2.dmi'
	icon_state = "inventory"
	crafting_id = "rev_smoll"

	shoot_delay = 4

	automatic = FALSE

	bullet_count_max = 6

	shoot_sounds = list('sound/weapons/revolver_heavy/shoot.ogg')

	view_punch = 10

	size = SIZE_1
	weight = WEIGHT_2

	slowdown_mul_held = HELD_SLOWDOWN_REVOLVER

	bullet_length_min = 10
	bullet_length_best = 29
	bullet_length_max = 30

	bullet_diameter_min = 10
	bullet_diameter_best = 10.9
	bullet_diameter_max = 11

	value = 170

	heat_per_shot = 0.05
	heat_max = 0.1

/obj/item/weapon/ranged/bullet/revolver/commander/small/get_static_spread() //Base spread
	return 1

/obj/item/weapon/ranged/bullet/revolver/commander/small/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.07 - (0.4 * L.get_skill_power(SKILL_RANGED)))

/obj/item/weapon/ranged/bullet/revolver/commander/big
	name = "\improper Револьвер заводского производства"
	desc = "Старый, заводской револьвер."
	desc_extended = "Некая жгучая смесь двух миров - дробовиков и револьверов. Стреляет достаточно больно, чаще всего - одного выстрела хватает на одну цель. Расчитан под 44-ый калибр и имеет при себе барабан на 6 патрон."
	icon = 'icons/obj/item/weapons/ranged/revolver/44_3.dmi'
	icon_state = "inventory"

	shoot_delay = 1

	automatic = FALSE

	bullet_count_max = 6

	shoot_sounds = list('sound/weapons/revolver_heavy/shoot.ogg')

	view_punch = 10

	size = SIZE_3
	weight = WEIGHT_4

	slowdown_mul_held = HELD_SLOWDOWN_REVOLVER

	bullet_length_min = 10
	bullet_length_best = 29
	bullet_length_max = 30

	bullet_diameter_min = 10
	bullet_diameter_best = 10.9
	bullet_diameter_max = 11

	value = 170

	heat_per_shot = 0.02
	heat_max = 0.1

/obj/item/weapon/ranged/bullet/revolver/commander/big/get_static_spread() //Base spread
	return 0.01

/obj/item/weapon/ranged/bullet/revolver/commander/big/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.02 - (0.05 * L.get_skill_power(SKILL_RANGED)))