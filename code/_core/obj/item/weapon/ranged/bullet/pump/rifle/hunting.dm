/obj/item/weapon/ranged/bullet/pump/rifle/hunting
	name = "\improper Старая дедовская болтовка"
	desc = "Для любителей перезаряжаться после каждого выстрела."
	desc_extended = "Древняя как мир гладкоствольная винтовка, вмещающая в себя всего один патрон. Когда-то, вы делали такие вместо со своим дедом/батей, а теперь их используют почти везде."
	icon = 'icons/obj/item/weapons/ranged/rifle/hunting.dmi'
	icon_state = "inventory"
	value = 40
	crafting_id = "samopal"

	shoot_delay = 2

	automatic = FALSE

	bullet_count_max = 1

	bullet_length_min = 90
	bullet_length_best = 99
	bullet_length_max = 100

	bullet_diameter_min = 12
	bullet_diameter_best = 12.7
	bullet_diameter_max = 13

	shoot_sounds = list('sound/weapons/rifle_heavy/shoot.ogg')

	can_wield = TRUE

	view_punch = 12

	pump_sound = 'sound/weapons/gun_slide3.ogg'

	slowdown_mul_held = HELD_SLOWDOWN_RIFLE

	size = SIZE_5
	weight = WEIGHT_4

/obj/item/weapon/ranged/bullet/pump/rifle/hunting/get_static_spread() //Base spread
	return 0

/obj/item/weapon/ranged/bullet/pump/rifle/hunting/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.01 - (0.01 * L.get_skill_power(SKILL_RANGED)))


/obj/item/weapon/ranged/bullet/pump/rifle/hunting/get_bullet_inaccuracy(var/mob/living/L,var/atom/target,var/obj/projectile/P,var/inaccuracy_modifier)

	var/distance = get_dist(L,target)

	if(distance <= 3)
		return TILE_SIZE*0.5 //No using snipers at close range.

	if(distance <= VIEW_RANGE*0.5)
		return max(0,1 - L.get_skill_power(SKILL_PRECISION)) * ((VIEW_RANGE*0.5)/get_dist(L,target)) * TILE_SIZE*0.5

	return max(0,1 - L.get_skill_power(SKILL_PRECISION))*(0.1+0.9*(get_dist(L,target) - VIEW_RANGE*0.5)) * (L.client && L.client.is_zoomed ? 0.25 : 1)