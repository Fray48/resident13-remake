/obj/item/weapon/ranged/bullet/magazine/pistol/nailgun
	name = "\improper ��������"
	desc = "���� ����� ���������� ����-�� � �����"
	desc_extended = "����� ������������ ����������, ������ �������������� ��� ������� ������."
	icon = 'icons/obj/item/weapons/ranged/pistol/nailgun.dmi'
	icon_state = "inventory"
	value = 140

	shoot_delay = 8

	automatic = TRUE

	shoot_sounds = list('sound/weapons/gyrojet/shoot.ogg')

	view_punch = 2

	slowdown_mul_held = HELD_SLOWDOWN_REVOLVER

	size = SIZE_2
	weight = WEIGHT_3

	heat_per_shot = 0
	heat_max = 0

	bullet_length_min = 25
	bullet_length_best = 30
	bullet_length_max = 31

	bullet_diameter_min = 4
	bullet_diameter_best = 4.6
	bullet_diameter_max = 5

/obj/item/weapon/ranged/bullet/magazine/pistol/nailgun/get_static_spread() //Base spread
	return 0.005

/obj/item/weapon/ranged/bullet/magazine/pistol/nailgun/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.10 - (0.15 * L.get_skill_power(SKILL_RANGED)) )