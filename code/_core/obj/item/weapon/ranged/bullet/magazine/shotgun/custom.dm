/obj/item/weapon/ranged/bullet/magazine/shotgun/custom
	name = "\improper 12g Double Action"
	desc = "For when you want to clear a room."
	desc_extended = "����������� �������������� ��������. ����� ������ �������� ��������, �� ���������� ������� ����."
	icon = 'icons/obj/item/weapons/ranged/rifle/auto_shotgun.dmi'
	icon_state = "inventory"
	value = 210

	shoot_delay = 3

	automatic = FALSE

	shoot_sounds = list('sound/weapons/12gauge/shoot.ogg')

	can_wield = TRUE
	wield_only = TRUE

	view_punch = 18

	slowdown_mul_held = HELD_SLOWDOWN_RIFLE

	size = SIZE_2
	weight = WEIGHT_3

	heat_per_shot = 0.05
	heat_max = 0.15

	bullet_length_min = 18
	bullet_length_best = 18.5
	bullet_length_max = 19

	bullet_diameter_min = 18
	bullet_diameter_best = 18.5
	bullet_diameter_max = 19

	value = 300

	ai_heat_sensitivity = 0.75

/obj/item/weapon/ranged/bullet/magazine/shotgun/custom/get_base_spread()
	return 0.08

/obj/item/weapon/ranged/bullet/magazine/shotgun/custom/get_static_spread() //Base spread
	return 0.002

/obj/item/weapon/ranged/bullet/magazine/shotgun/custom/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.05 - (0.1 * L.get_skill_power(SKILL_RANGED)))