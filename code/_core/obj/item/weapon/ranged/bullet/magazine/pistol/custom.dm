/obj/item/weapon/ranged/bullet/magazine/pistol/handgun
	name = "����������� ��������"
	desc = "��������� �� ������ ���������� ������ ������� ��������."
	desc_extended = "������������ �������� ��� ������ .45 ACP. ����� �������� � ���� �������� �� 9 ���������. ��������� ��������� ������ �������."
	value = 30
	icon = 'icons/obj/item/weapons/ranged/pistol/handgun.dmi'
	shoot_delay = 2
	shoot_sounds = list('sound/weapons/45/shoot.ogg')

	view_punch = 6

	automatic = FALSE

	size = SIZE_2
	weight = WEIGHT_2

	heat_per_shot = 0.1
	heat_max = 0.8

	bullet_length_min = 20
	bullet_length_best = 23
	bullet_length_max = 24

	bullet_diameter_min = 11
	bullet_diameter_best = 11.43
	bullet_diameter_max = 12

/obj/item/weapon/ranged/bullet/magazine/pistol/handgun/get_static_spread() //Base spread
	return 0.04

/obj/item/weapon/ranged/bullet/magazine/pistol/handgun/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.08 - (0.08 * L.get_skill_power(SKILL_RANGED)))