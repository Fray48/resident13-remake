/obj/item/weapon/melee/resident/crowbar
	name = "Лом"
	desc = "Против лома нет приёма"
	desc_extended = "Лучший помощник в взломе...И ломании черепов."
	icon = 'icons/obj/item/weapons/melee/resident/crowbar.dmi'
	damage_type = /damagetype/melee/club/crowbar/

	value = 20

/obj/item/weapon/melee/resident/golf
	name = "Клюшка для гольфа"
	desc = "Главный двигатель сюжета TLOU2!"
	desc_extended = "Когда-то, эта крошка использовалась для игр, пускай и не очень популярных по миру. Сейчас, в принципе, ничего не изменилось, только игры заменила борьба за выживание."
	icon = 'icons/obj/item/weapons/melee/resident/golf.dmi'
	damage_type = /damagetype/melee/club/golf/

	value = 15

	size = SIZE_5
	weight = WEIGHT_4

/obj/item/weapon/melee/resident/bone
	name = "Дубинка из костей"
	desc = "У-у-у, страшно!"
	desc_extended = "Для любителей оккультики."
	icon = 'icons/obj/item/weapons/melee/resident/bone.dmi'
	damage_type = /damagetype/melee/club/bone/

	value = 15

/obj/item/weapon/melee/resident/pipe
	name = "Металлическая труба"
	desc = "Не самое практичное оружие"
	desc_extended = "Мало-удобная, металлическая балка, которой с лёгкостью можно расквасить мозги незащищённой цели."
	icon = 'icons/obj/item/weapons/melee/resident/pipe.dmi'
	damage_type = /damagetype/melee/club/pipe/

	value = 15

/obj/item/weapon/melee/resident/spear_stles
	name = "металлическое копьё"
	desc = "Тупое. Как ты."
	desc_extended = "Отлично подойдёт, если нужно кого-нибудь отвлечь. Убить кого-то им - на вряд ли возможно."
	icon = 'icons/obj/item/weapons/melee/swords/spear_weak.dmi'
	damage_type = /damagetype/melee/spear_weak/
	damage_type_thrown = /damagetype/melee/spear/thrown_weak
	crafting_id = "spearstels"

	attack_delay = 5
	attack_delay_max = 12

	value = 10

/obj/item/weapon/melee/sword/resident/knife
	name = "самодельный нож"
	desc = "Удобен для разделывания добычи"
	desc_extended = "Лучший помощник в вскрытии туш поверженных тварей и диких животных...Кто о чём."
	icon = 'icons/obj/item/weapons/melee/swords/knife.dmi'
	damage_type = /damagetype/melee/sword/knife/
	crafting_id = "knoif"

	attack_delay = 3
	attack_delay_max = 10

	value = 10

/obj/item/weapon/melee/sword/resident/axe
	name = "самодельный топор"
	desc = "Лучшее оружие для точных хирургических операций"
	desc_extended = "Отличный выбор для тех людей, что обожают сочетать в оружии убойную мощь и скорость."
	icon = 'icons/obj/item/weapons/melee/resident/axe.dmi'
	damage_type = /damagetype/melee/axe/
	crafting_id = "axe"

	attack_delay = 2
	attack_delay_max = 5

	value = 10

	item_slot = SLOT_GROIN_O | SLOT_TORSO_B
	worn_layer = LAYER_MOB_CLOTHING_BACK
	slot_icons = TRUE

/obj/item/weapon/melee/sword/resident/axe/can_be_worn(var/mob/living/advanced/owner,var/obj/hud/inventory/I)
	return TRUE

/obj/item/weapon/melee/sword/resident/axe/electric
	name = "самодельный топор"
	desc = "Лучшее оружие для точных хирургических операций"
	desc_extended = "Отличный выбор для тех людей, что обожают сочетать в оружии убойную мощь и скорость."
	icon = 'icons/obj/item/weapons/melee/resident/axe_mod.dmi'
	damage_type = /damagetype/melee/axe_electric

	attack_delay = 8
	attack_delay_max = 12

/obj/item/weapon/melee/resident/bat
	name = "Бейсбольная бита"
	desc = "Старая деревянная бита"
	icon = 'icons/obj/item/weapons/melee/resident/bat.dmi'
	damage_type = /damagetype/melee/club/bat
	crafting_id = "bat"

	attack_delay = 5
	attack_delay_max = 10

	value = 10

/obj/item/weapon/melee/resident/bat/up1
	name = "Бейсбольная бита"
	desc = "Старая деревянная бита, напичканная гвоздями"
	icon = 'icons/obj/item/weapons/melee/resident/bat_upgrade.dmi'
	damage_type = /damagetype/melee/club/bat_upgraded

	attack_delay = 5
	attack_delay_max = 10

	value = 10

/obj/item/weapon/melee/resident/bat/up2
	name = "Бейсбольная бита"
	desc = "Старая деревянная бита, поверх которой натянули проволоку"
	icon = 'icons/obj/item/weapons/melee/resident/bat_upgrade2.dmi'
	damage_type = /damagetype/melee/club/bat_upgraded2

	attack_delay = 5
	attack_delay_max = 10

	value = 10

/obj/item/weapon/melee/resident/bat/up3
	name = "Бейсбольная бита"
	desc = "Старая деревянная бита, к которой прицепили лезвие от пилы"
	icon = 'icons/obj/item/weapons/melee/resident/bat_upgrade3.dmi'
	damage_type = /damagetype/melee/club/bat_upgraded3

	attack_delay = 5
	attack_delay_max = 10

	value = 10

/obj/item/weapon/melee/sword/resident/machete
	name = "ржавое мачете"
	desc = "Отличное оружие для прорезания сквозь густую листву, или толпы мертвецов"
	desc_extended = "С лёгкостью проломит достаточно толстую деревянную заслонку. Или, если понадобится - череп."
	icon = 'icons/obj/item/weapons/melee/resident/machete.dmi'
	damage_type = /damagetype/melee/sword/machete/

	attack_delay = 4
	attack_delay_max = 10

	value = 10