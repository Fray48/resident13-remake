obj/item/weapon/melee
	name = "Melee Weapon"
	desc = "A melee weapon"

	block_defense_rating = DEFAULT_BLOCK_MELEE

	attack_delay = 10