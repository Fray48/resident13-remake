/obj/item/experience_scroll/skill
	var/skill
	var/experience_amount

/obj/item/experience_scroll/skill/get_examine_list()
	. = ..()
	. += div("notice","Reading \the [src.name] will grant [experience_amount]xp into [skill].")

/obj/item/experience_scroll/skill/gain_knowledge(var/mob/living/advanced/A)
	if(!skill || !experience_amount)
		return FALSE

	A.visible_message(span("notice","\The [A.name] reads \the [src.name], taking in the knowledge..."),span("notice","You read \the [src.name], taking in the knowledge..."))
	add_item_count(-1)

	A.add_skill_xp(skill,experience_amount)
	return ..()

/obj/item/experience_scroll/skill/melee
	name = "мануал по ближнему бою"
	skill = SKILL_MELEE
	experience_amount = 5000
	value = 2000

/obj/item/experience_scroll/skill/unarmed
	name = "мануал по рукопашному бою"
	skill = SKILL_UNARMED
	experience_amount = 5000
	value = 2000


/obj/item/experience_scroll/skill/ranged
	name = "мануал по стрельбе с гладкоствольного оружия"
	skill = SKILL_RANGED
	experience_amount = 5000
	value = 2000

/obj/item/experience_scroll/skill/precision
	name = "журнал для тренировки глаз"
	skill = SKILL_PRECISION
	experience_amount = 5000
	value = 2000

/obj/item/experience_scroll/skill/survival
	name = "книга выживания"
	skill = SKILL_SURVIVAL
	experience_amount = 5000
	value = 2000



/obj/item/experience_scroll/skill/magic
	name = "scroll of magic"
	skill = SKILL_MAGIC
	experience_amount = 5000
	value = 2000

/obj/item/experience_scroll/skill/medicine
	name = "медицинский справочник"
	skill = SKILL_MEDICINE
	experience_amount = 5000
	value = 2000