/obj/item/experience_scroll

	name = "книга"
	desc = "Полная новых знаний литература."
	desc_extended = "Небольшой мануал с различной относительно полезной информацией. Может помочь с обучением чему-либо."

	icon = 'icons/obj/item/experience_scroll.dmi'
	icon_state = "book5"

	var/time_to_read = 600 //Сколько читаем

	item_count_current = 1
	item_count_max = 10

	weight = WEIGHT_0
	size = SIZE_2

	value = -1

/obj/item/experience_scroll/PostInitialize() //Random shelf.
	. = ..()
	icon_state = "book[rand(1,6)]"
	return .

/obj/item/experience_scroll/proc/gain_knowledge(var/mob/living/advanced/A)
	play('sound/ui/friendly.ogg',get_turf(src),range_max=VIEW_RANGE)
	return TRUE

/obj/item/experience_scroll/click_self(var/mob/caller)
	INTERACT_CHECK

	if(!is_advanced(caller) || !caller.is_player_controlled())
		caller.to_chat(span("warning","You don't know how to read this..."))
		return TRUE

	var/mob/living/advanced/A = caller

	if(!A.allow_experience_gains)
		caller.to_chat(span("warning","You don't know how to read this..."))
		return TRUE

	PROGRESS_BAR(caller,src,time_to_read,.proc/gain_knowledge,caller,caller)
	PROGRESS_BAR_CONDITIONS(caller,src,.proc/can_read,caller,caller)

	return TRUE

/obj/item/experience_scroll/proc/can_read(var/mob/caller)
	var/mob/living/L = caller

	if(get_dist(L,src) > 1)
		L.to_chat("You're too far away!")
		return FALSE
	if(!src.loc || !is_inventory(src.loc))
		L.to_chat(span("warning","You need to be holding \the [src.name] in order to read \the [src.name]!"))
		return FALSE

	if(L.dead)
		L.to_chat(span("warning","Well, good time to read!"))
		return FALSE
	if(L.has_status_effect(list(PARALYZE,SLEEP,STAGGER,STUN,CONFUSED)))
		L.to_chat(span("warning","How can someone read in this state?.."))
		return FALSE

	if(L.move_delay >= 0)
		L.to_chat(span("warning","You need to stand still in order to read \the [src.name]!"))
		return FALSE

	return TRUE