/obj/item/magazine/rifle_556
	name = "\improper магазин калибра 5.56"
	desc = "Используется для B35!"
	desc_extended = "Contains ammunition for a ranged weapon. Make sure you're trying to use the right caliber."
	icon = 'icons/obj/item/magazine/556_rifle.dmi'
	icon_state = "556"
	bullet_count_max = 25

	weapon_whitelist = list(
		/obj/item/weapon/ranged/bullet/magazine/rifle/standard = TRUE
	)

	ammo = /obj/item/bullet_cartridge/rifle_223

	bullet_length_min = 40
	bullet_length_best = 45
	bullet_length_max = 46

	bullet_diameter_min = 5.5
	bullet_diameter_best = 5.56
	bullet_diameter_max = 5.6

	size = SIZE_2
	weight = WEIGHT_2

/obj/item/magazine/rifle_556/update_icon()
	icon_state = "[initial(icon_state)]_[length(stored_bullets) ? 1 : 0]"
	return ..()

/obj/item/magazine/rifle_556_1
	name = "\improper магазин калибра 5.56"
	desc = "Используется для М40!"
	desc_extended = "Contains ammunition for a ranged weapon. Make sure you're trying to use the right caliber."
	icon = 'icons/obj/item/magazine/556_1_rifle.dmi'
	icon_state = "556"
	bullet_count_max = 30

	weapon_whitelist = list(
		/obj/item/weapon/ranged/bullet/magazine/rifle/nt_carbine = TRUE
	)

	ammo = /obj/item/bullet_cartridge/rifle_223

	bullet_length_min = 40
	bullet_length_best = 45
	bullet_length_max = 46

	bullet_diameter_min = 5.5
	bullet_diameter_best = 5.56
	bullet_diameter_max = 5.6

	size = SIZE_2
	weight = WEIGHT_2

/obj/item/magazine/rifle_556_1/update_icon()
	icon_state = "[initial(icon_state)]_[length(stored_bullets) ? 1 : 0]"
	return ..()