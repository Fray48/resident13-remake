/obj/item/magazine/destro_smg
	name = "\improper 9mm smp magazine"
	desc = "IT'S NOT A CLIP. IT'S A MAGAZINE."
	desc_extended = "Contains ammunition for a ranged weapon. Make sure you're trying to use the right caliber."
	icon = 'icons/obj/item/magazine/resident_smg.dmi'
	icon_state = "uzi"
	crafting_id = "uzi_mag"
	bullet_count_max = 16

	weapon_whitelist = list(
		/obj/item/weapon/ranged/bullet/magazine/smg/destro = TRUE
	)

	ammo = /obj/item/bullet_cartridge/pistol_9mm

	bullet_length_min = 16
	bullet_length_best = 19
	bullet_length_max = 20

	bullet_diameter_min = 8.5
	bullet_diameter_best = 9
	bullet_diameter_max = 9.5

	size = SIZE_2
	weight = WEIGHT_2

/obj/item/magazine/destro_smg/update_icon()

	if(length(stored_bullets))
		icon_state = "[initial(icon_state)]_[round(length(stored_bullets),2)]"
	else
		icon_state = "[initial(icon_state)]_0"

	..()