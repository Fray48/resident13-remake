/obj/item/container/pill/bicaridine
	name = "bicaridine pill (20u)"

/obj/item/container/pill/bicaridine/Generate()
	reagents.add_reagent(/reagent/medicine/bicaridine,20)
	return ..()

/obj/item/container/pill/kelotane
	name = "kelotane pill (20u)"

/obj/item/container/pill/kelotane/Generate()
	reagents.add_reagent(/reagent/medicine/kelotane,20)
	return ..()

/obj/item/container/pill/dylovene
	name = "dylovene pill (20u)"

/obj/item/container/pill/dylovene/Generate()
	reagents.add_reagent(/reagent/medicine/dylovene,20)
	return ..()

/obj/item/container/pill/iron
	name = "iron pill (20u)"

/obj/item/container/pill/iron/Generate()
	reagents.add_reagent(/reagent/iron,20)
	return ..()

/obj/item/container/pill/space_drugs
	name = "space drugs (5u)"
	icon_state = "circle"

/obj/item/container/pill/space_drugs/Generate()
	reagents.add_reagent(/reagent/drug/space,5)
	return ..()


/obj/item/container/pill/space_dust
	name = "space dust (10u)"
	icon_state = "circle"

/obj/item/container/pill/space_dust/Generate()
	reagents.add_reagent(/reagent/drug/liberty_dust,10)
	return ..()


/obj/item/container/pill/omnizine
	name = "omnizine pill (20u)"

/obj/item/container/pill/omnizine/Generate()
	reagents.add_reagent(/reagent/medicine/omnizine,20)
	return ..()

/obj/item/container/pill/gtrava
	name = "green herb"
	icon = 'icons/omnibus_general_v1a.dmi'
	icon_state = "g"
	crafting_id = "g"

/obj/item/container/pill/gtrava/Generate()
	reagents.add_reagent(/reagent/medicine/bicaridine,15)
	return ..()

/obj/item/container/pill/rtrava
	name = "red herb"
	icon = 'icons/omnibus_general_v1a.dmi'
	icon_state = "r"
	crafting_id = "r"

/obj/item/container/pill/rtrava/Generate()
	reagents.add_reagent(/reagent/medicine/kelotane,15)
	return ..()

/obj/item/container/pill/btrava
	name = "blue herb"
	icon = 'icons/omnibus_general_v1a.dmi'
	icon_state = "b"
	crafting_id = "b"

/obj/item/container/pill/btrava/Generate()
	reagents.add_reagent(/reagent/medicine/dylovene,20)
	return ..()

/obj/item/container/pill/spray
	name = "medical spray"
	icon = 'icons/stalker/sPRAY.dmi'
	icon_state = "1"

/obj/item/container/pill/spray/Generate()
	reagents.add_reagent(/reagent/medicine/health_potion,30)
	return ..()

/obj/item/container/pill/rgb
	name = "RGB"
	icon = 'icons/omnibus_general_v1a.dmi'
	icon_state = "grb"

/obj/item/container/pill/rgb/Generate()
	reagents.add_reagent(/reagent/medicine/health_potion,30)
	return ..()