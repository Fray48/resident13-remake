/obj/item/container/food/package/junkfood
	name = "junk food"
	icon = 'icons/obj/item/consumable/food/processed.dmi'

/obj/item/container/food/package/junkfood/chips
	name = "Пачка риса"
	desc = "Пачка сухого риса."
	desc_extended = "Не очень вкурсно - но хоть что-то."
	icon_state = "chips"

/obj/item/container/food/package/junkfood/chips/Initialize()
	name = pick("Рис","Рис")
	return ..()

/obj/item/container/food/package/junkfood/chips/Generate()
	reagents.add_reagent(/reagent/nutrition/junk/chips,20)
	reagents.add_reagent(/reagent/salt,10)
	return ..()


/obj/item/container/food/package/junkfood/candy
	name = "Сникерс"
	desc = "Не тормози - сникерсни."
	desc_extended = "Что ещё ты хочешь тут увидеть?"
	icon_state = "candy"

/obj/item/container/food/package/junkfood/candy/Generate()
	reagents.add_reagent(/reagent/nutrition/junk/chocolate,10)
	reagents.add_reagent(/reagent/nutrition/sugar,5)
	return ..()



/obj/item/container/food/package/junkfood/jerky
	name = "Паштет"
	desc = "Обычный консервированный паштет."
	desc_extended = "Самое питательное, что можно найти."
	icon_state = "jerky"

/obj/item/container/food/package/junkfood/jerky/Generate()
	reagents.add_reagent(/reagent/nutrition/junk/jerky,25)
	reagents.add_reagent(/reagent/salt,5)
	return ..()

/obj/item/container/food/package/junkfood/raisins
	name = "Сырок"
	desc = "Просто сырок"
	desc_extended = "Что ещё ты хочешь?"
	icon_state = "raisins"

/obj/item/container/food/package/junkfood/raisins/Generate()
	reagents.add_reagent(/reagent/nutrition/junk/raisins,10)
	reagents.add_reagent(/reagent/salt,5)
	reagents.add_reagent(/reagent/nutrition/sugar,5)
	return ..()


/obj/item/container/food/package/junkfood/cake
	name = "Шоколадная плитка"
	desc = "Вкусно, но не сказать что питательно."
	desc_extended = "Лучший друг любого ребёнка."
	icon_state = "cake"

/obj/item/container/food/package/junkfood/cake/Generate()
	reagents.add_reagent(/reagent/nutrition/junk/cake,10)
	reagents.add_reagent(/reagent/nutrition/junk/cream_filling,5)
	reagents.add_reagent(/reagent/nutrition/sugar,5)
	return ..()


/obj/item/container/food/package/junkfood/syndicate
	name = "Консерва"
	desc = "Что покоится внутри?"
	desc_extended = "Что ещё ты хочешь?"
	icon_state = "syndicate"

/obj/item/container/food/package/junkfood/syndicate/Generate()
	reagents.add_reagent(/reagent/nutrition/junk/cake,10)
	reagents.add_reagent(/reagent/medicine/omnizine,10)
	return ..()

/obj/item/container/food/package/junkfood/cheese_chips
	name = "Галеты"
	desc = "Пачка сырных галет."
	desc_extended = "Когда под руками нету чипсов."
	icon_state = "cheesie_honkers"

/obj/item/container/food/package/junkfood/cheese_chips/Generate()
	reagents.add_reagent(/reagent/nutrition/junk/chips,15)
	reagents.add_reagent(/reagent/salt,5)
	reagents.add_reagent(/reagent/nutrition/junk/cheese_powder,25)
	return ..()