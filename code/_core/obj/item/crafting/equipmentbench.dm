/obj/item/crafting/equipment
	name = "portable workstation"
	icon = 'icons/obj/item/ore.dmi'
	icon_state = "ammo_bench"
	anchored = TRUE
	collision_flags = FLAG_COLLISION_WALL

	inventories = list(

		/obj/hud/inventory/crafting/slotB1,
		/obj/hud/inventory/crafting/slotB2,
		/obj/hud/inventory/crafting/slotB3,

		/obj/hud/inventory/crafting/slotC2,

		/obj/hud/inventory/crafting/result
	)

	crafting_id = "equipmentbench"

	crafting_type = /recipe/equipment/

	value = 25