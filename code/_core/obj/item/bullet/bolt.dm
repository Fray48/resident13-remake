/obj/item/bullet_cartridge/bolt
	name = "crossbow bolt"
	desc = "A crudely designed bolt meant for crossbows."
	icon = 'icons/obj/item/bullet/bolt.dmi'
	item_count_max = 6
	item_count_max_icon = 6
	icon_state = "bolt"

	bullet_diameter = 5.56
	bullet_length = 45

	worn_layer = LAYER_MOB_CLOTHING_BELT
	item_slot = SLOT_GROIN_O

	projectile = /obj/projectile/bullet/bolt
	damage_type_bullet = /damagetype/ranged/bullet/crossbow_bolt

	projectile_speed = BULLET_SPEED_PISTOL_HEAVY

	size = 0.5
	weight = 1
	value = 0.5

/obj/item/bullet_cartridge/bolt/Generate()
	item_count_current = 6
	return ..()

/obj/item/bullet_cartridge/bolt/can_be_worn(var/mob/living/advanced/owner,var/obj/hud/inventory/I)
	return TRUE

/obj/item/bullet_cartridge/arrow
	name = "arrow"
	icon_state = "arrow"
	icon = 'icons/obj/item/bullet/arrow.dmi'

	projectile = /obj/projectile/bullet/bolt
	damage_type_bullet = /damagetype/ranged/bow/

	projectile_speed = BULLET_SPEED_PISTOL_HEAVY

	item_count_max = 5
	item_count_max_icon = 5

	bullet_length = -1
	bullet_diameter = -1

	size = SIZE_1
	value = 2

/obj/item/bullet_cartridge/arrow/Generate()
	item_count_current = item_count_max
	update_sprite()
	return ..()

/obj/item/bullet_cartridge/arrow/spend_bullet(var/mob/caller,var/bonus_misfire_chance=0)
	src.projectile_count = item_count_current
	qdel(src)
	return src