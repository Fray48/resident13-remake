/obj/structure/interactive/resident/g_herb
	name = "green herb"
	icon = 'icons/stalker/fallout/flora.dmi'
	icon_state = "g"
	plane = -6

/obj/structure/interactive/resident/g_herb/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","� �������� ������ �� ����� ������ �� �������!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	spawn()
		var/obj/item/new_item = new /obj/item/container/pill/gtrava(src)
		INITIALIZE(new_item)
		GENERATE(new_item)
		new_item.update_sprite()
		I.add_object(new_item,TRUE)
		P.to_chat(span("notice","�� �������� ��������"))
		qdel(src)

	return TRUE

/obj/structure/interactive/resident/b_herb
	name = "blue herb"
	icon = 'icons/stalker/fallout/flora.dmi'
	icon_state = "b"
	plane = -6

/obj/structure/interactive/resident/b_herb/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","� �������� ������ �� ����� ������ �� �������!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	spawn()
		var/obj/item/new_item = new /obj/item/container/pill/btrava(src)
		INITIALIZE(new_item)
		GENERATE(new_item)
		new_item.update_sprite()
		I.add_object(new_item,TRUE)
		P.to_chat(span("notice","�� �������� ��������"))
		qdel(src)

	return TRUE

/obj/structure/interactive/resident/r_herb
	name = "red herb"
	icon = 'icons/stalker/fallout/flora.dmi'
	icon_state = "r"
	plane = -6

/obj/structure/interactive/resident/r_herb/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","� �������� ������ �� ����� ������ �� �������!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	spawn()
		var/obj/item/new_item = new /obj/item/container/pill/rtrava(src)
		INITIALIZE(new_item)
		GENERATE(new_item)
		new_item.update_sprite()
		I.add_object(new_item,TRUE)
		P.to_chat(span("notice","�� �������� ��������"))
		qdel(src)

	return TRUE

/obj/structure/interactive/resident/g_herb_pot
	name = "pot with green herb"
	icon = 'icons/omnibus_general_v1a.dmi'
	icon_state = "g_pot"
	plane = -6

	var/picked = FALSE

/obj/structure/interactive/resident/g_herb_pot/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","� �������� ������ �� ����� ������ �� �������!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	if(picked == FALSE)
		spawn()
			var/obj/item/new_item = new /obj/item/container/pill/gtrava(src)
			INITIALIZE(new_item)
			GENERATE(new_item)
			new_item.update_sprite()
			I.add_object(new_item,TRUE)
			P.to_chat(span("notice","�� �������� ��������"))
			icon_state = "pot"
			picked = TRUE

	return TRUE

/obj/structure/interactive/resident/r_herb_pot
	name = "pot with red herb"
	icon = 'icons/omnibus_general_v1a.dmi'
	icon_state = "r_pot"
	plane = -6

	var/picked = FALSE

/obj/structure/interactive/resident/r_herb_pot/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","� �������� ������ �� ����� ������ �� �������!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	if(picked == FALSE)
		spawn()
			var/obj/item/new_item = new /obj/item/container/pill/rtrava(src)
			INITIALIZE(new_item)
			GENERATE(new_item)
			new_item.update_sprite()
			I.add_object(new_item,TRUE)
			P.to_chat(span("notice","�� �������� ��������"))
			icon_state = "pot"
			picked = TRUE

	return TRUE

/obj/structure/interactive/resident/b_herb_pot
	name = "pot with blue herb"
	icon = 'icons/omnibus_general_v1a.dmi'
	icon_state = "b_pot"
	plane = -6

	var/picked = FALSE

/obj/structure/interactive/resident/b_herb_pot/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","� �������� ������ �� ����� ������ �� �������!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	if(picked == FALSE)
		spawn()
			var/obj/item/new_item = new /obj/item/container/pill/btrava(src)
			INITIALIZE(new_item)
			GENERATE(new_item)
			new_item.update_sprite()
			I.add_object(new_item,TRUE)
			P.to_chat(span("notice","�� �������� ��������"))
			icon_state = "pot"
			picked = TRUE

	return TRUE