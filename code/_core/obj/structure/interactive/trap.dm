/obj/structure/interactive/rastyazhka
	name = "��������"
	icon = 'icons/stalker/metro-2/ph_traps.dmi'
	icon_state = "booby"
	plane = -10

/obj/structure/interactive/rastyazhka/Crossed(var/atom/movable/O)

	. = ..()

	if(is_living(O))
		var/mob/living/L = O
		play('sound/items/drop/ring.ogg',get_turf(L))
		L.visible_message(span("danger","[O.name] �������� ��������!"))
		create_alert(VIEW_RANGE,L,L,ALERT_LEVEL_COMBAT)
		explode(get_turf(src),2,src,src)
		qdel(src)

	return .

/obj/structure/interactive/rastyazhka/clicked_on_by_object(var/mob/caller,object,location,control,params)

	INTERACT_CHECK

	var/atom/A = check_interactables(caller,object,location,control,params)
	if(A && A.clicked_on_by_object(caller,object,location,control,params))
		return TRUE

	if(istype(object, /obj/item/weapon/melee/sword/resident/knife) && prob(40))
		caller.visible_message(span("notice","[caller.name] ���������� ��������!"))
		qdel(src)
	if(istype(object, /obj/item/weapon/melee/tool/wirecutters) && prob(95))
		caller.visible_message(span("notice","[caller.name] ���������� ��������!"))
		qdel(src)


	else
		caller.visible_message(span("danger","[caller.name] �������� ������� �������!"))
		explode(get_turf(src),2,src,src)
		qdel(src)

