/obj/item/key
	name = "key"
	desc = "Used to unlock things."
	icon = 'icons/items.dmi'
	icon_state = "keys"
	var/access = list()

/obj/item/key/New()
	..()
	icon_state = "key[rand(1, 4)]"


/obj/item/key/base
	name = "key"
	desc = "Used to unlock things. With label << Base >>, on the other side <<Don't forget to lock the Doors>>"
	access = list(BASE)

/obj/item/key/warehouse
	name = "key"
	desc = "Used to unlock things. With label << Warehouse >> and ont the other side of it <<I'll fuck ya if u...steal something>>"
	access = list(WAREHOUSE)

/obj/item/key/hydro
	name = "key"
	desc = "Used to unlock things. With label << Hydro >>"
	access = list(HYDRO)

/obj/item/key/bar
	name = "key"
	desc = "Used to unlock things. With label << Bar >>"
	access = list(BAR)

/obj/item/key/civil
	name = "key"
	desc = "Used to unlock things. With label << Civilian >> on the other side of it reminder: <<Locking toilets is probably bad idea>>"
	access = list(CIVIL)

/obj/item/key/head
	name = "key"
	desc = "Used to unlock things. With label << General >>, with tiny words: <<Congrats, now you're a head of that base>> "
	access = list(BASE, WAREHOUSE, HYDRO, BAR, CIVIL)

/obj/item/key/base/outlands_1
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_1)

/obj/item/key/base/outlands_2
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_2)

/obj/item/key/base/outlands_3
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_3)

/obj/item/key/base/outlands_4
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_4)

/obj/item/key/base/outlands_5
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_5)

/obj/item/key/base/outlands_6
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_6)

/obj/item/key/base/outlands_7
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_7)

/obj/item/key/base/outlands_8
	name = "key"
	desc = "Used to unlock things. It is old one"
	access = list(OUTLANDS_8)

/obj/item/key/fuel_rod
	name = "EREDC"
	icon = 'icons/items3.dmi'
	icon_state = "battery1"
	desc = "Emergency Reserve Energy Door Cell."

/obj/item/key/fuel_rod/New()

	return FALSE