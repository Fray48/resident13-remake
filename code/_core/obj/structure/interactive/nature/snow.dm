obj/structure/scenery/pinetrees
	name = "pinetree"
	icon = 'icons/obj/structure/flora/pinetrees.dmi'
	icon_state = "pine_1"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -16
	pixel_y = 0
	layer = LAYER_LARGE_OBJ

	plane = PLANE_TREE

	mouse_opacity = 0

	has_transparency_marker = TRUE


obj/structure/scenery/pinetrees/New()
	..()
	icon_state = "pine_[rand(1,3)]"

obj/structure/scenery/snow_tree
	name = "dead tree"
	icon = 'icons/128tree.dmi'
	icon_state = "tree_winter_64x128_1"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -16
	pixel_y = 0
	layer = LAYER_LARGE_OBJ

	plane = PLANE_TREE

	mouse_opacity = 0

	has_transparency_marker = TRUE

obj/structure/scenery/snow_tree2
	name = "dead tree"
	icon = 'icons/96tree.dmi'
	icon_state = "96tree"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -32
	pixel_y = 10
	layer = LAYER_LARGE_OBJ

	plane = PLANE_TREE

	mouse_opacity = 0

	has_transparency_marker = TRUE

obj/structure/scenery/snow_tree3
	name = "dead tree"
	icon = 'icons/treeees.dmi'
	icon_state = "booo"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -16
	pixel_y = 0
	layer = LAYER_LARGE_OBJ

	plane = PLANE_TREE

	mouse_opacity = 0

	has_transparency_marker = TRUE

obj/structure/scenery/snow_grass
	name = "grass"
	icon = 'icons/obj/structure/flora/snowflora.dmi'
	icon_state = "snowgrass1"


obj/structure/scenery/snow_grass/New()
	..()
	icon_state = "snowgrass[rand(1,3)]"


obj/structure/scenery/snow_bush
	name = "berry bush"
	icon = 'icons/obj/structure/flora/snowflora.dmi'
	icon_state = "snowbush1"
	layer = LAYER_GROUND_SCENERY

obj/structure/scenery/snow_bush/New()
	..()
	icon_state = "snowbush[rand(1,3)]"

obj/structure/scenery/evergreen2
	name = "tree"
	icon = 'icons/96tree.dmi'
	icon_state = "hbtree96"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -32
	pixel_y = 10
	layer = LAYER_LARGE_OBJ

	plane = PLANE_TREE

	mouse_opacity = 0

	has_transparency_marker = TRUE

obj/structure/scenery/big_tree
	name = "tree"
	icon = 'icons/164tree.dmi'
	icon_state = "hdtree96"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -32
	pixel_y = 10
	layer = LAYER_LARGE_OBJ

	mouse_opacity = 0

	has_transparency_marker = TRUE

	plane = PLANE_TREE

obj/structure/scenery/big_tree2
	name = "tree"
	icon = 'icons/164tree.dmi'
	icon_state = "vhtree152"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -32
	pixel_y = 10
	layer = LAYER_LARGE_OBJ

	mouse_opacity = 0

	has_transparency_marker = TRUE

	plane = PLANE_TREE

obj/structure/scenery/evergreen3
	name = "tree"
	icon = 'icons/96tree.dmi'
	icon_state = "mbtree96"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	pixel_x = -16
	pixel_y = 0
	layer = LAYER_LARGE_OBJ

	mouse_opacity = 0

	has_transparency_marker = TRUE

	plane = PLANE_TREE

obj/structure/scenery/drygrass
	name = "dry grass"
	icon = 'icons/stalker/some_stuff/plants.dmi'
	icon_state = "drygrass0"
	plane = -5