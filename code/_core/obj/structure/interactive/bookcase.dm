var/global/list/stored_bookcase_phrases = list(
	"...��� ��������� �������� �������...",
	"...��� ������, ���� �� ��������� ������ ������ � ����� ������...",
	"...���� �� ��������� � ������...",
	"...��� ������ ������ ������ ����� ��� ���� ��������...",
	"...��� ��� ���������� ������������ ������������ � ������� ���...",
	"...������ ����� - ���...",
	"...����� ����...��� ������..."
)


/obj/structure/interactive/bookcase
	name = "������ ������� �����"
	desc = "��� �� ������ ��� ����� �������?"
	desc_extended = "������ ������� �����. �����������, ��� � ��� �� ��������� �� ���������."
	icon = 'icons/obj/structure/bookcase.dmi'
	icon_state = "book"
	plane = -5
	collision_flags = FLAG_COLLISION_WALL
	bullet_block_chance = 60

	var/chance_of_scroll = 0

	density = TRUE

/obj/structure/interactive/bookcase/Generate()

	if(prob(25))
		chance_of_scroll = rand(1,5)
		icon_state = "[initial(icon_state)]_[chance_of_scroll]"

	return ..()

/obj/structure/interactive/bookcase/clicked_on_by_object(var/mob/caller,var/atom/object,location,control,params)

	if(!is_inventory(object))
		return ..()

	if(can_search_case(caller))
		caller.to_chat(span("notice","�� ����� ���������� [src.name]..."))
		PROGRESS_BAR(caller,src,SECONDS_TO_DECISECONDS(5),.proc/search_case,caller)
		PROGRESS_BAR_CONDITIONS(caller,src,.proc/can_search_case,caller)

	return TRUE

/obj/structure/interactive/bookcase/proc/can_search_case(var/mob/caller)

	INTERACT_CHECK

	if(prob(5))
		caller.to_chat(span("notice",pick(stored_bookcase_phrases)))

	return TRUE

/obj/structure/interactive/bookcase/proc/search_case(var/mob/caller)

	if(chance_of_scroll <= 0)
		caller.to_chat(span("warning","...������, ��� ����� �� �����������."))
		return TRUE

	if(prob(chance_of_scroll))
		var/turf/T = get_turf(src)
		CREATE_LOOT(/loot/skill_scroll,T)
		caller.to_chat(span("notice","���! ������ �� ������!"))
		chance_of_scroll = 0
	else
		chance_of_scroll--
		caller.to_chat(span("warning","...�� ������ �� �����."))

	return TRUE