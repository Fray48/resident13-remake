/obj/structure/interactive/crate/open
	open = TRUE

/obj/structure/interactive/crate/closet
	name = "closet"
	icon = 'icons/obj/structure/closet.dmi'
	icon_state = "closet"

/obj/structure/interactive/crate/closet/anchored
	anchored = TRUE

/obj/structure/interactive/crate/closet/anchored/open
	anchored = TRUE
	open = TRUE

/obj/structure/interactive/crate/engineering
	icon_state = "engineering"

/obj/structure/interactive/crate/medical
	icon_state = "medical"

/obj/structure/interactive/crate/coffin
	name = "coffin"
	icon_state = "coffin"

/obj/structure/interactive/crate/closet/anchored/loot
	anchored = TRUE

/obj/structure/interactive/crate/closet/anchored/loot/Generate()

	switch(rand(1,32))
		if(1 to 2)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/weapon/melee/resident/crowbar,src.loc)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/weapon/melee/torch/flashlight/maglight,src.loc)
		if(3)
			for(var/i=1,i<=rand(1),i++)
				new /mob/living/advanced/npc/beefman(src.loc)
		if(4 to 6)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/storage/irp,src.loc)
		if(7)
			for(var/i=1,i<=rand(2,4),i++)
				CREATE(/obj/item/storage/pillbottle/kelotane,src.loc)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/storage/ammo/slug_23,src.loc)
		if(8 to 10)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/magazine/pistol_tranq_11m,src.loc)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/weapon/ranged/bullet/magazine/pistol/tranq,src.loc)
		if(10 to 14)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/radio,src.loc)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/weapon/melee/resident/golf,src.loc)
		if(14 to 18)
			for(var/i=1,i<=rand(2),i++)
				CREATE(/obj/item/magazine/pistol_45,src.loc)
		if(18 to 22)
			for(var/i=1,i<=rand(1,3),i++)
				CREATE(/obj/item/storage/irp,src.loc)
		if(22 to 26)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/storage/ifak,src.loc)
			for(var/i=1,i<=rand(2,4),i++)
				CREATE(/obj/item/container/beaker/alcohol/vodka,src.loc)
		if(26 to 30)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/storage/ifak,src.loc)
			for(var/i=1,i<=rand(2,3),i++)
				CREATE(/obj/item/magazine/samopal,src.loc)
		if(30 to 32)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/container/spray/resident,src.loc)


	return ..()

/obj/structure/interactive/crate/anchored/special/loot
	icon_state = "milcrate"
	desc = "Старый ящик с экипировкой"
	desc_extended = "Сарый ящик с экипировкой и инструментарием. Такие скидывали в качестве продовольствия в первые дни. Внутри может быть что-нибудь интересное..."
	anchored = TRUE

/obj/structure/interactive/crate/anchored/special/loot/Generate()

	switch(rand(1,32))
		if(1 to 2)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/resident/crafting/instruments,src.loc)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/resident/crafting/mag_recharger,src.loc)
		if(3)
			for(var/i=1,i<=rand(1),i++)
				new /obj/item/resident/crafting/scope(src.loc)
		if(4 to 6)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/weapon/melee/sword/resident/axe/electric,src.loc)
		if(7)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/weapon/melee/resident/bat/up2,src.loc)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/storage/ammo/slug_23,src.loc)
		if(8 to 10)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/resident/crafting/mag_recharger,src.loc)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/resident/crafting/blueprint,src.loc)
		if(10 to 14)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/radio,src.loc)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/weapon/melee/resident/golf,src.loc)
		if(14 to 18)
			for(var/i=1,i<=rand(2),i++)
				CREATE(/obj/item/magazine/pistol_45,src.loc)
		if(18 to 22)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/weapon/melee/resident/bat/up3,src.loc)
		if(22 to 26)
			for(var/i=1,i<=rand(1,2),i++)
				CREATE(/obj/item/storage/ifak,src.loc)
			for(var/i=1,i<=rand(2,4),i++)
				CREATE(/obj/item/container/beaker/alcohol/vodka,src.loc)
		if(26 to 30)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/weapon/ranged/bullet/magazine/rifle/nt_lmg,src.loc)
			for(var/i=1,i<=rand(2,3),i++)
				CREATE(/obj/item/magazine/samopal,src.loc)
		if(30 to 32)
			for(var/i=1,i<=rand(1),i++)
				CREATE(/obj/item/weapon/ranged/bullet/magazine/shotgun/custom,src.loc)


	return ..()