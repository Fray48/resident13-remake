#define TICK_USAGE world.tick_usage
#define TICK_LIMIT 90

#define CHECK_TICK ( (TICK_USAGE > TICK_LIMIT) ? stoplag() : FALSE )

#define CHECK_TICK_A(limit,max_delays) \
	if(limit && world.tick_usage > limit) { \
		var/safety_count=0; \
		while(world.tick_usage > limit && (max_delays <= safety_count || !max_delays)) {\
			safety_count++; \
			sleep(TICK_LAG * (world.tick_usage/limit)); \
		}\
	}

